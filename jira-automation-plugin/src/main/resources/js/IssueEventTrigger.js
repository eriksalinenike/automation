(function() {
    AJS.$(AJS).bind("AJS.automation.form.loaded", function(e, context) {

        new AJS.MultiSelect({
            element:AJS.$("#jiraEventId", context),
            itemAttrDisplayed: "label"
        });

        var $restrictionRuleEnablers = AJS.$(".enable-restriction-rules", context);

        $restrictionRuleEnablers.change(function (e) {

            e.preventDefault();

            var $enabler = AJS.$(this);
            var $target = AJS.$($enabler.data('target'), context);

            if ($enabler.is(':checked')) {
                $target.show();
            } else {
                $target.hide();
            }

        });

        var $restrictAuthors = AJS.$("#restrictEventAuthors" ,context);
        $restrictAuthors.change(function(e) {
            e.preventDefault();
            if($restrictAuthors.is(":checked")) {
                AJS.$(".event-authors", context).show();
                $restrictionRuleEnablers.change();
            } else {
                AJS.$(".event-authors", context).hide();
            }
        });
        $restrictAuthors.change();

        new AJS.SingleSelect({
            element: AJS.$("#eventSyncProcessingOverride", context),
            itemAttrDisplayed: "label"
        });

        AJS.automation.AjaxUserPickerUtils.singleSelect("specificUser", context);
        AJS.automation.AjaxGroupPickerUtils.singleSelect("specificGroup", context);
    });
})();