(function () {
    AJS.namespace("AJS.automation.JQLAutoComplete");

    AJS.automation.JQLAutoComplete.initialize = function(fieldID, errorID, context) {
        var field = AJS.$('#'+fieldID, context);
        var jqlAutocompleteInitialized = field.data("jql-init");
        if(jqlAutocompleteInitialized || field.length === 0) {
            return;
        }

        AJS.$.ajax({
            url: AJS.contextPath() + "/rest/api/2/jql/autocompletedata",
            success: function (response) {
                var jqlFieldNames = response.visibleFieldNames;
                var jqlFunctionNames = response.visibleFunctionNames;
                var jqlReservedWords = response.jqlReservedWords;

                var jqlAutoComplete = JIRA.JQLAutoComplete({
                    field: field, //'jqltext',
                    parser: JIRA.JQLAutoComplete.MyParser(jqlReservedWords),
                    queryDelay: .65,
                    jqlFieldNames: jqlFieldNames,
                    jqlFunctionNames: jqlFunctionNames,
                    minQueryLength: 0,
                    allowArrowCarousel: true,
                    autoSelectFirst: false,
                    errorID: errorID //'jqlerrormsg'
                });

                // ensure we don't submit anything
                field.unbind("keypress", submitOnEnter);
                jqlAutoComplete.buildResponseContainer();
                jqlAutoComplete.parse(field.text());
                jqlAutoComplete.updateColumnLineCount();

                field.click(function () {
                    jqlAutoComplete.dropdownController.hideDropdown();
                });
            }
        });

        field.data("jql-init", true);
    };

    AJS.$(AJS).bind("AJS.automation.form.loaded", function(e, context) {
        AJS.automation.JQLAutoComplete.initialize("jiraJqlExpression","jql-errormsg", context);
    });
})();