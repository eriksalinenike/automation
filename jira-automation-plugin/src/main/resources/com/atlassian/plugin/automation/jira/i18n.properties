automation.jira.jql=JQL expression
automation.jira.jql.desc=JQL expression to fetch issues.
automation.jira.jql.filter.desc=Option JQL expression that will be used to filter issues. Only matching issues will be processed.
automation.jira.jql.invalid=Invalid JQL provided

automation.jira.jql.maxResults=Limit results
automation.jira.jql.maxResults.desc=Limits the maximum amount of items returned by the filter
automation.jira.jql.maxResults.invalid=Invalid number of results

automation.jira.eventid=Issue Event
automation.jira.eventid.desc=Select an issue event that will trigger this rule (only applies to com.atlassian.jira.event.issue.IssueEvent).
automation.jira.eventid.jql.desc=Optional JQL expression used to determine whether an issue should be processed
automation.jira.eventid.invalid=Please specify a valid issue event.

automation.jira.issuepropertyset.keys.required=Issue Property Set Event property key(s) cannot be empty
automation.jira.event.propertyKeys=Property Key(s)
automation.jira.event.propertyKeys.desc=EntityProperty Key(s) to trigger from (delimited by commas)

automation.jira.comment=Comment
automation.jira.restrict.visibility=Comment Visibility
automation.jira.comment.desc=The following variables are available to insert dynamic content into the comment: $issue, $reporter, $project
automation.jira.comment.invalid=Comment is invalid
automation.jira.comment.notification=Send notification?

automation.jira.commentRoleId=Commenter role ID
automation.jira.commentRoleId.desc=Role ID of the commenter, see
automation.jira.commentSecurityLevel.invalid=Invalid comment security level specified.

automation.jira.actionid=Workflow Transition
automation.jira.actionid.desc=Please note that this must be a valid transition for the issues returned by the trigger.
automation.jira.actionid.invalid=Action ID is invalid

automation.jira.jiraTransitionFields=Transition fields
automation.jira.jiraTransitionFields.descOpt=Optional fields to set on transition. Defined as field_id=field value pairs on new line
automation.jira.jiraTransitionFields.invalid=Fields parameters are invalid
automation.jira.transition.notification=Disable notification for this transition?
automation.jira.transition.conditions=Skip condition checking
automation.jira.transition.conditions.desc=This is useful i.e when the transition is hidden from user. Note: setting only applies on JIRA 6.3 and higher
automation.jira.transition.workflowNotFound=Unable to find configured workflow. Is it renamed/removed? Please edit the action to fix this issue.

automation.jira.jiraEditFields=Edit fields
automation.jira.jiraEditFields.desc=Fields to be editted. Defined as field_id=field value pairs on new line. For example: summary=My summary
automation.jira.jiraEditFields.invalid=Field parameters are invalid
automation.jira.jiraEditFields.empty=Field parameters cannot be empty
automation.jira.jiraAllowVariableExpansion=Allow variable expansion
automation.jira.variableExpansion=Variable expansion
automation.jira.jiraAllowVariableExpansion.desc=If you allow variable expansion, you are able to manipulate fields in a more dynamic way.
automation.jira.jiraAllowVariableExpansion.explanation_1=You can use the default fields, like: $issue.summary, $issue.description and $issue.assignee.name
automation.jira.jiraAllowVariableExpansion.explanation_2=You can use custom fields, by providing their ID, for example: $customfields.get(10000)
automation.jira.jiraAllowVariableExpansion.explanation_3=To assign a value to a customfield, you can use: customfield_10000=Example: $issue.summary
automation.jira.jiraAllowVariableExpansion.explanation_4=To prepend a value from a customfield before the summary, you could do use: summary=$customfields.get(10000) $issue.summary
automation.jira.jiraAllowVariableExpansion.explanation_5=Text and number fields are currently the only supported custom fields
automation.jira.edit.notification=Send notification?

automation.jira.group.membership.invalid=Please select a valid group.
automation.jira.group.membership=Group Membership
automation.jira.group.membership.desc=Restrict the users who can be set as the assignee to a particular group.
automation.jira.group.membership.unrestricted=Unrestricted

automation.jira.users.excluded=Excluded users
automation.jira.users.excluded.desc=A list of users that will not get issues assigned
automation.jira.users.excluded.invalid=The following users couldn't be found :

automation.jira.event.sync.processing.override=Synchronous processing
automation.jira.event.sync.no.override=No override, use default settings
automation.jira.event.sync.override=Synchronous event processing
automation.jira.event.async.override=Asynchronous event processing
automation.jira.event.sync.processing.override.desc=This option allows the global ''Synchronous processing'' setting to be overridden for this rule. If asynchronous processing is selected, this rule will be processed asynchronously which can improve overall performance. However, if the system is under heavy load, then it is possible for Automation to experience delays in between when events occur and when their triggered rules are executed. Selecting synchronous processing will prevent these delays but may decrease overall performance under normal circumstances.
automation.jira.event.sync.view.enabled=The event will be processed synchronously
automation.jira.event.async.view.enabled=The event will be processed asynchronously
automation.jira.event.restrict.author=Restrict which users can trigger this event?
automation.jira.event.restrict.info=This trigger will invoke its actions if any of the following conditions are true.
automation.jira.event.restrict.view.info=This trigger will only invoke its actions if the following conditions are met:
automation.jira.event.restrict.reporter.is.user=The user triggering the issue event is the reporter of the issue
automation.jira.event.restrict.reporter.is.not.user=The event will not be triggered by the reporter of the issue
automation.jira.event.restrict.assignee.is.user=The user triggering the issue event is the assignee of the issue
automation.jira.event.restrict.assignee.is.not.user=The event will not be triggered by the assignee of the issue
automation.jira.event.restrict.cf.is.user=The user triggering the issue event is a member of the {0}{1}{2} user custom field values for the issue in question
automation.jira.event.restrict.specific.is.user=The user triggering the issue event is {0}
automation.jira.event.restrict.specific.is.in.group=The user triggering the issue event is in {0}
automation.jira.event.restrict.specific.is.not.in.group=The user triggering the issue event is not in {0}


automation.jira.event.enable.reporter.rules=Enable restrict reporter rules?
automation.jira.event.enable.assignee.rules=Enable restrict assignee rules?

automation.jira.event.restrict.reporter=Event author is the reporter of the issue
automation.jira.event.restrict.not.reporter=Event author is NOT the reporter of the issue
automation.jira.event.restrict.assignee=Event author is the assignee of the issue
automation.jira.event.restrict.not.assignee=Event author is NOT the assignee of the issue
automation.jira.event.restrict.cf=Event Author User Customfield
automation.jira.event.restrict.cf.desc=This trigger will be executed if the event author is a member of the user customfield specified here.
automation.jira.event.restrict.specific.user=Event Author
automation.jira.event.restrict.specific.user.desc=This trigger will be executed if the event author matches the user specified here.
automation.jira.event.restrict.specific.group=Event Author Group Membership
automation.jira.event.restrict.in.group=Event author is in the selected group
automation.jira.event.restrict.not.in.group=Event author is NOT in the selected group
automation.jira.event.restrict.cf.none=None
automation.jira.event.restrict.cf.error=Please select a valid user custom field.
automation.jira.event.restrict.user.error=No user with username ''{0}'' could be found.
automation.jira.event.restrict.group.error=No group with name ''{0}'' could be found.

automation.jira.editLabels.empty=At least one label is needed
automation.jira.editLabels.invalid=Label is invalid
automation.jira.editLabels.add=Labels to add
automation.jira.editLabels.add.desc=These labels will be added if they are not already present
automation.jira.editLabels.remove=Labels to remove
automation.jira.editLabels.remove.desc=These labels will be removed if they are present
automation.jira.editLabels.notification=Send notification?
automation.jira.editLabels.jiraHandleLabelsCaseInsensitive=Handle labels case insensitive?

automation.jira.deleteIssue.notification=Send notification?

automation.jira.publishEvent.desc=Select an event which will be published. NOTE: this can cause unexpected behavior if the event needs an extra data, such as Issue Commented event.

automation.action.editProfile.field.empty=User property field cannot be empty
automation.action.editProfile.desc=Full key used in JIRA to for the user property. Available values are {0}. Only String properties are currently supported.
automation.action.editProfile.propertyKey=Property key
automation.action.editProfile.propertyValue=Property value
automation.action.editProfile.shouldOverwrite=Should this value overwrite the non-default value set by user?
automation.action.editProfile.propertyValue.desc=Value of the property to be set.

automation.action.deleteIssue=Delete Issue
automation.action.deleteIssue.desc=<strong>IMPORTANT:</strong> This action <strong>will delete</strong> all issues returned by the input trigger.

automation.action.deleteAttachments=Delete Attachments
automation.action.deleteAttachments.desc=<strong>IMPORTANT:</strong> This action <strong>will delete</strong> all attachments of the issues returned by the input trigger.
automation.action.deleteAttachments.addComment=Comment visibility
automation.action.deleteAttachments.addComment.desc=Comment will be added while deleting attachments and will contain filenames of the deleted attachments
automation.action.deleteAttachments.ignore=Skip attachments pattern
automation.action.deleteAttachments.ignore.desc=Attachments matching these patterns will be skipped (i.e. not deleted). The patterns use standard wildcard pattern and can be separated using comma. For example: *.properties,logo*.*ng

automation.action.setPropertyAction.propertyKey=Property key
automation.action.setPropertyAction.propertyKey.desc=Full issue property key to be set.
automation.action.setPropertyAction.propertyValue=Property value
automation.action.setPropertyAction.propertyValue.desc=Value of the property to be set.
automation.action.setPropertyAction.field.empty=Issue property field cannot be empty
