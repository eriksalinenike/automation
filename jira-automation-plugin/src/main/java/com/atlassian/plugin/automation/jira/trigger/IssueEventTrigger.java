package com.atlassian.plugin.automation.jira.trigger;

import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.event.type.EventTypeManager;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.UserField;
import com.atlassian.jira.plugin.userformat.UserFormats;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.plugin.automation.core.EventTrigger;
import com.atlassian.plugin.automation.core.TriggerContext;
import com.atlassian.plugin.automation.core.auditlog.AuditString;
import com.atlassian.plugin.automation.core.auditlog.DefaultAuditString;
import com.atlassian.plugin.automation.core.trigger.TriggerConfiguration;
import com.atlassian.plugin.automation.jira.util.AutomationIssueEventUtil;
import com.atlassian.plugin.automation.jira.util.JqlMatcherService;
import com.atlassian.plugin.automation.util.ErrorCollection;
import com.atlassian.plugin.automation.util.ParameterUtil;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import javax.annotation.Nullable;
import javax.inject.Inject;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.atlassian.plugin.automation.jira.util.Constants.CONFIG_COMPLETE_KEY;
import static com.atlassian.plugin.automation.util.ParameterUtil.singleValue;

@Scanned
public class IssueEventTrigger implements EventTrigger<Issue>
{
    private static final Logger log = Logger.getLogger(IssueEventTrigger.class);

    private static final String PROFILE_LINK_USER_FORMAT_TYPE = "profileLinkWithAvatar";

    public static final String EVENT_ID_KEY = "jiraEventId";
    public static final String EVENT_JQL_KEY = "jiraJqlExpression";
    public static final String USER_CF_ID = "userCustomFieldId";
    public static final String SPECIFIC_USER = "specificUser";
    public static final String SPECIFIC_GROUP = "specificGroup";
    public static final String IN_GROUP = "currentIsInGroup";
    public static final String EVENT_SYNC_OVERRIDE = "eventSyncProcessingOverride";
    public static final String RESTRICT_EVENT_AUTHORS = "restrictEventAuthors";

    private static final String RESOURCE_KEY = "com.atlassian.plugin.automation.jira-automation-plugin:jira-config-resources";
    private static final String JIRA_EVENTS = "jiraEvents";

    private final SoyTemplateRenderer soyTemplateRenderer;
    private final EventTypeManager eventTypeManager;
    private final CustomFieldManager customFieldManager;
    private final UserManager userManager;
    private final GroupManager groupManager;
    private final I18nResolver i18n;
    private final UserFormats userFormats;
    private final JqlMatcherService jqlMatcherService;
    private final Set<Long> eventIds = Sets.newHashSet();

    private String jql;
    private Predicate<IssueEvent> issueEventUserPredicate;

    @Inject
    public IssueEventTrigger(
            @ComponentImport final SoyTemplateRenderer soyTemplateRenderer,
            @ComponentImport final EventTypeManager eventTypeManager,
            @ComponentImport final UserManager userManager,
            @ComponentImport final GroupManager groupManager,
            @ComponentImport final CustomFieldManager customFieldManager,
            @ComponentImport final I18nResolver i18nResolver,
            @ComponentImport final UserFormats userFormats,
            final JqlMatcherService jqlMatcherService)
    {
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.eventTypeManager = eventTypeManager;
        this.customFieldManager = customFieldManager;
        this.userManager = userManager;
        this.groupManager = groupManager;
        this.i18n = i18nResolver;
        this.userFormats = userFormats;
        this.jqlMatcherService = jqlMatcherService;
    }

    @Override
    public void init(TriggerConfiguration config)
    {
        eventIds.addAll(AutomationIssueEventUtil.transformEventIds(config.getParameters().get(EVENT_ID_KEY)));
        jql = singleValue(config, EVENT_JQL_KEY);
        issueEventUserPredicate = new IssueEventUserPredicate(config, customFieldManager, groupManager);
        final String eventSyncEnabled = getEventSyncEnabledValue(config);
        if (StringUtils.isNotBlank(eventSyncEnabled))
        {
            config.getParameters().put(ParameterUtil.RULE_EVENT_SYNC_ENABLED, Collections.singletonList(eventSyncEnabled));
        }
    }

    @Override
    public Iterable<Issue> getItems(TriggerContext context, ErrorCollection errorCollection)
    {
        IssueEvent issueEvent = (IssueEvent) context.getSourceEvent();
        if (issueEvent != null)
        {
            if (eventIds.contains(issueEvent.getEventTypeId()) && issueEventUserPredicate.apply(issueEvent))
            {
                final Issue issue = jqlMatcherService.getMatchedIssue(context, errorCollection, jql, issueEvent.getIssue().getId());
                if (issue != null)
                {
                    return Collections.singleton(issue);
                }
            }
        }

        return Collections.emptyList();
    }

    @Override
    public AuditString getAuditLog()
    {
        return new DefaultAuditString(String.format("Issue Event trigger for events %s", Arrays.toString(eventIds.toArray())));
    }

    @Override
    public String getConfigurationTemplate(TriggerConfiguration triggerConfiguration, final String actor)
    {
        try
        {
            final Map<String, Object> context = Maps.newHashMap();
            ParameterUtil.transformParams(context, triggerConfiguration);

            context.put(JIRA_EVENTS, AutomationIssueEventUtil.getRenderableEvents(eventTypeManager.getEventTypes(),
                    triggerConfiguration, EVENT_ID_KEY, userManager.getUserByName(actor)));

            final Iterable<CustomField> userCFs = Iterables.filter(customFieldManager.getCustomFieldObjects(), new Predicate<CustomField>()
            {
                @Override
                public boolean apply(@Nullable final CustomField input)
                {
                    return input != null && input.getCustomFieldType() instanceof UserField;
                }
            });
            if (!Iterables.isEmpty(userCFs))
            {
                context.put("userCFs", userCFs);
            }

            if (triggerConfiguration != null)
            {
                final String eventSyncEnabled = getEventSyncEnabledValue(triggerConfiguration);
                if (StringUtils.isNotBlank(eventSyncEnabled))
                {
                    context.put(ParameterUtil.RULE_EVENT_SYNC_ENABLED, eventSyncEnabled);
                }
                else
                {
                    context.remove(ParameterUtil.RULE_EVENT_SYNC_ENABLED);
                }
                final String specificUser = singleValue(triggerConfiguration.getParameters(), SPECIFIC_USER);
                if (StringUtils.isNotBlank(specificUser))
                {
                    final ApplicationUser user = userManager.getUserByName(specificUser.trim());
                    if (user != null)
                    {
                        context.put(SPECIFIC_USER, user);
                    }
                }
            }

            return soyTemplateRenderer.render(RESOURCE_KEY, "Atlassian.Templates.Automation.JIRA.issueEvent", context);
        }
        catch (SoyException e)
        {
            log.error("Error rendering template", e);
            return "Unable to render configuration form. Consult your server logs or administrator.";
        }
    }

    @Override
    public String getViewTemplate(final TriggerConfiguration config, final String actor)
    {
        try
        {
            final Map<String, Object> context = Maps.newHashMap();
            ParameterUtil.transformParams(context, config);
            Set<Long> eventIds = Sets.newHashSet();
            if (config != null && config.getParameters().containsKey(EVENT_ID_KEY))
            {
                eventIds = AutomationIssueEventUtil.transformEventIds(config.getParameters().get(EVENT_ID_KEY));
            }
            final List<String> eventNames = Lists.newArrayList();
            for (Long eventId : eventIds)
            {
                eventNames.add(i18n.getText(eventTypeManager.getEventType(eventId).getTranslatedName(userManager.getUserByName(actor))));
            }
            Collections.sort(eventNames);
            context.put(JIRA_EVENTS, eventNames);

            if (config != null && config.getParameters().containsKey(EVENT_SYNC_OVERRIDE))
            {
                final String eventSyncEnabled = getEventSyncEnabledValue(config);
                if (StringUtils.isNotBlank(eventSyncEnabled))
                {
                    context.put(ParameterUtil.RULE_EVENT_SYNC_ENABLED, eventSyncEnabled);
                }
            }
            
            final IssueEventUserPredicate issueEventUserPredicate = new IssueEventUserPredicate(config, customFieldManager, groupManager);
            final CustomField restrictingCustomField = issueEventUserPredicate.getRestrictingCustomField();
            if (restrictingCustomField != null)
            {
                context.put("userCF", restrictingCustomField.getFieldName());
            }
            if (issueEventUserPredicate.getSpecificUser() != null)
            {
                context.put("specificUser", userFormats.formatter(PROFILE_LINK_USER_FORMAT_TYPE).formatUserkey(issueEventUserPredicate.getSpecificUser(), "retricted-user-" + config.getId()));
            }

            return soyTemplateRenderer.render(CONFIG_COMPLETE_KEY, "Atlassian.Templates.Automation.JIRA.issueEventView", context);
        }
        catch (SoyException e)
        {
            log.error("Error rendering template", e);
            return "Unable to render configuration form. Consult your server logs or administrator.";
        }
    }

    @Override
    public ErrorCollection validateAddConfiguration(I18nResolver i18n, Map<String, List<String>> params, final String actor)
    {
        final ErrorCollection errorCollection = new ErrorCollection();

        if (!params.containsKey(EVENT_ID_KEY))
        {
            errorCollection.addError(EVENT_ID_KEY, i18n.getText("automation.jira.eventid.invalid"));
        }
        else
        {
            for (String eventId : params.get(EVENT_ID_KEY))
            {
                if (StringUtils.isBlank(eventId) || !StringUtils.isNumeric(eventId))
                {
                    errorCollection.addError(EVENT_ID_KEY, i18n.getText("automation.jira.eventid.invalid"));
                    break;
                }
            }
        }

        final String jqlString = singleValue(params, EVENT_JQL_KEY);
        if (StringUtils.isNotBlank(jqlString))
        {
            errorCollection.addErrorCollection(jqlMatcherService.validateJql(actor, EVENT_JQL_KEY, jqlString));
        }

        //validate restrict event author options.
        if (Boolean.parseBoolean(singleValue(params, RESTRICT_EVENT_AUTHORS)))
        {
            final String customFieldId = singleValue(params, USER_CF_ID);
            if (StringUtils.isNotBlank(customFieldId))
            {
                final CustomField customFieldObject = customFieldManager.getCustomFieldObject(customFieldId);
                if (customFieldObject == null || !(customFieldObject.getCustomFieldType() instanceof UserField))
                {
                    errorCollection.addError(USER_CF_ID, i18n.getText("automation.jira.event.restrict.cf.error"));
                }
            }
            final String specificUser = singleValue(params, SPECIFIC_USER);
            if (StringUtils.isNotBlank(specificUser))
            {
                final ApplicationUser user = userManager.getUserByName(specificUser.trim());
                if (user == null)
                {
                    errorCollection.addError(SPECIFIC_USER, i18n.getText("automation.jira.event.restrict.user.error", specificUser.trim()));
                }
            }
            final String specificGroup = singleValue(params, SPECIFIC_GROUP);
            if (StringUtils.isNotBlank(specificGroup) && !groupManager.groupExists(specificGroup.trim()))
            {
                errorCollection.addError(SPECIFIC_GROUP, i18n.getText("automation.jira.event.restrict.group.error", specificGroup.trim()));
            }

        }


        return errorCollection;
    }

    @Override
    public String getEventClassName()
    {
        return "com.atlassian.jira.event.issue.IssueEvent";
    }
    
    private String getEventSyncEnabledValue(final TriggerConfiguration triggerConfiguration)
    {
        final String eventSyncOverride = singleValue(triggerConfiguration.getParameters(), EVENT_SYNC_OVERRIDE);
        if (StringUtils.isNotBlank(eventSyncOverride) && eventSyncOverride.equals("sync"))
        {
            return "true";
        }
        else if (StringUtils.isNotBlank(eventSyncOverride) && eventSyncOverride.equals("async"))
        {
            return "false";
        }
        return null;
    }
}
