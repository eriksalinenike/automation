package com.atlassian.plugin.automation.jira.action;

import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueInputParametersImpl;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.jira.util.JiraUrlCodec;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.TransitionOptions;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.plugin.automation.core.Action;
import com.atlassian.plugin.automation.core.action.ActionConfiguration;
import com.atlassian.plugin.automation.core.auditlog.AuditString;
import com.atlassian.plugin.automation.core.auditlog.DefaultAuditString;
import com.atlassian.plugin.automation.jira.util.ErrorCollectionUtil;
import com.atlassian.plugin.automation.jira.util.ParameterParserUtil;
import com.atlassian.plugin.automation.util.ErrorCollection;
import com.atlassian.plugin.automation.util.ParameterUtil;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.opensymphony.workflow.loader.ActionDescriptor;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import javax.annotation.Nullable;
import javax.inject.Inject;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static com.atlassian.jira.workflow.TransitionOptions.Builder;
import static com.atlassian.plugin.automation.jira.util.Constants.CONFIG_COMPLETE_KEY;
import static com.atlassian.plugin.automation.jira.util.ParameterParserUtil.getFieldsMap;
import static com.atlassian.plugin.automation.util.ParameterUtil.singleValue;

/**
 * Implements transition of an issue
 */
@Scanned
public class TransitionIssueAction implements Action<Issue>
{
    private static final Logger log = Logger.getLogger(TransitionIssueAction.class);

    public static final String ACTION_ID_KEY = "jiraActionId";
    public static final String TRANSITION_FIELDS_KEY = "jiraTransitionFields";
    public static final String DIVIDER = "|||";
    public static final String DISABLE_NOTIFICATION_KEY = "disableTransitionNotification";
    public static final String SKIP_CONDITIONS = "skipConditions";
    public static final int JIRA_63_BUILD_NUMBER = 6328;

    private final UserManager userManager;
    private final IssueService issueService;
    private final SoyTemplateRenderer soyTemplateRenderer;
    private final WorkflowManager workflowManager;
    private final I18nResolver i18nResolver;
    private final boolean supportsSkipCondition;
    private String actionId;
    private Map<String, String[]> fieldsMap;
    private boolean sendNotification;
    private boolean skipConditions;

    @Inject
    public TransitionIssueAction(
            @ComponentImport final UserManager userManager,
            @ComponentImport final IssueService issueService,
            @ComponentImport final SoyTemplateRenderer soyTemplateRenderer,
            @ComponentImport final WorkflowManager workflowManager,
            @ComponentImport final I18nResolver i18nResolver,
            @ComponentImport final BuildUtilsInfo buildUtilsInfo)
    {
        this.userManager = userManager;
        this.issueService = issueService;
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.workflowManager = workflowManager;
        this.i18nResolver = i18nResolver;

        // poor man's reflection - JIRA 6.3 came with extra help on transitions, so if we don't have the right version don't even try
        supportsSkipCondition = buildUtilsInfo.getApplicationBuildNumber() >= JIRA_63_BUILD_NUMBER;
    }

    @Override
    public void init(ActionConfiguration config)
    {
        actionId = singleValue(config, ACTION_ID_KEY);
        fieldsMap = Maps.newHashMap();
        sendNotification = !Boolean.parseBoolean(singleValue(config, DISABLE_NOTIFICATION_KEY));
        skipConditions = supportsSkipCondition && Boolean.parseBoolean(singleValue(config, SKIP_CONDITIONS));
        final String fieldsText = singleValue(config, TRANSITION_FIELDS_KEY);
        if (!StringUtils.isBlank(fieldsText))
        {
            fieldsMap = getFieldsMap(fieldsText);
        }
    }

    @Override
    public void execute(String actor, Iterable<Issue> items, ErrorCollection errorCollection)
    {
        log.debug("Processing issues");
        final ApplicationUser user = userManager.getUserByName(actor);

        for (Issue issue : items)
        {
            log.debug("Processing the issue: " + issue.getKey());
            final Issue mutableIssue = issueService.getIssue(user, issue.getId()).getIssue();
            final com.atlassian.jira.util.ErrorCollection jiraErrorCollection = new SimpleErrorCollection();
            final IssueService.TransitionValidationResult transitionValidationResult;

            final IssueInputParametersImpl params = new IssueInputParametersImpl(fieldsMap);
            params.setSkipScreenCheck(true);

            // poor man's reflection - JIRA 6.3 came with this new method, so if we don't have the right version don't even try
            if (skipConditions)
            {
                final TransitionOptions trasitionOptions = new Builder().skipPermissions().skipValidators().setAutomaticTransition().skipConditions().build();
                transitionValidationResult = issueService.validateTransition(user,
                        mutableIssue.getId(), getTransitionId(actionId), params, trasitionOptions);
            }
            else
            {
                transitionValidationResult = issueService.validateTransition(user,
                        mutableIssue.getId(), getTransitionId(actionId), params);
            }

            if (transitionValidationResult.isValid())
            {
                transitionValidationResult.getAdditionInputs().put("sendBulkNotification", sendNotification);
                log.debug("Transitioning the issue to state: " + actionId + " || Send notification for the transition: " + sendNotification);
                final IssueService.IssueResult transitionResult = issueService.transition(user, transitionValidationResult);
                if (!transitionResult.isValid())
                {
                    jiraErrorCollection.addErrorCollection(transitionResult.getErrorCollection());
                }
            }
            else
            {
                jiraErrorCollection.addErrorCollection(transitionValidationResult.getErrorCollection());
            }
            if (jiraErrorCollection.hasAnyErrors())
            {
                log.error(String.format("Unable to transition issue '%s' using actor '%s': %s", issue.getKey(), actor, jiraErrorCollection));
                errorCollection.addErrorCollection(ErrorCollectionUtil.transform(jiraErrorCollection));
            }
        }
    }

    @Override
    public AuditString getAuditLog()
    {
        return new DefaultAuditString(String.format("Transition issue - Transitioned to workflow step with id %s. Fields set: '%s'",
                actionId, ParameterParserUtil.getFieldsMapString(fieldsMap)));
    }

    @Override
    public String getConfigurationTemplate(ActionConfiguration actionConfiguration, final String actor)
    {
        try
        {
            final Map<String, Object> context = Maps.newHashMap();
            ParameterUtil.transformParams(context, actionConfiguration);

            final Collection<JiraWorkflow> activeWorkflows = workflowManager.getActiveWorkflows();
            context.put("activeWorkflows", activeWorkflows);

            return soyTemplateRenderer.render(CONFIG_COMPLETE_KEY, "Atlassian.Templates.Automation.JIRA.transitionAction", context);
        }
        catch (SoyException e)
        {
            log.error("Error rendering template", e);
            return "Unable to render configuration form. Consult your server logs or administrator.";
        }
    }

    @Override
    public String getViewTemplate(final ActionConfiguration config, final String actor)
    {
        try
        {
            final Map<String, Object> context = Maps.newHashMap();
            ParameterUtil.transformParams(context, config);
            final String workflowName = getWorkflowName(ParameterUtil.singleValue(config, ACTION_ID_KEY));
            final Integer transitionId = getTransitionId(ParameterUtil.singleValue(config, ACTION_ID_KEY));

            final JiraWorkflow workflow = workflowManager.getWorkflow(workflowName);
            // in case the workflow got deleted or renamed, this may introduce NPE here
            if (workflow == null)
            {
                final ErrorCollection errors = new ErrorCollection();
                errors.addErrorMessage(i18nResolver.getText("automation.jira.transition.workflowNotFound"));
                context.put("errors", errors);
                return soyTemplateRenderer.render(CONFIG_COMPLETE_KEY, "Atlassian.Templates.Automation.JIRA.errorRenderingView", context);
            }
            final ActionDescriptor actionDescriptor = Iterables.find(workflow.getAllActions(), new Predicate<ActionDescriptor>()
            {
                @Override
                public boolean apply(@Nullable final ActionDescriptor input)
                {
                    return input != null && input.getId() == transitionId;
                }
            }, null);
            context.put("workflow", workflow);
            context.put("workflowNameUrlParam", JiraUrlCodec.encode(workflow.getName()));
            context.put("transition", actionDescriptor);
            context.put("transitionId", transitionId);

            return soyTemplateRenderer.render(CONFIG_COMPLETE_KEY, "Atlassian.Templates.Automation.JIRA.transitionActionView", context);
        }
        catch (SoyException e)
        {
            log.error("Error rendering template", e);
            return "Unable to render configuration form. Consult your server logs or administrator.";
        }
    }

    @Override
    public ErrorCollection validateAddConfiguration(I18nResolver i18n, Map<String, List<String>> params, final String actor)
    {
        final ErrorCollection errors = new ErrorCollection();
        if (!params.containsKey(ACTION_ID_KEY))
        {
            errors.addError(ACTION_ID_KEY, i18n.getText("automation.jira.actionid.invalid"));
            return errors;
        }

        if (getTransitionId(singleValue(params, ACTION_ID_KEY)) == -1)
        {
            errors.addError(ACTION_ID_KEY, i18n.getText("automation.jira.actionid.invalid"));
        }

        if (params.containsKey(TRANSITION_FIELDS_KEY))
        {
            // try to parse the fields
            if (getFieldsMap(singleValue(params, TRANSITION_FIELDS_KEY)) == null)
            {
                errors.addError(TRANSITION_FIELDS_KEY, i18n.getText("automation.jira.jiraTransitionFields.invalid"));
            }
        }
        return errors;
    }

    private String getWorkflowName(final String actionIdRaw)
    {
        return actionIdRaw.substring(0, actionIdRaw.indexOf(DIVIDER));
    }

    private Integer getTransitionId(final String actionIdRaw)
    {
        final String actionIdString = actionIdRaw.substring(actionIdRaw.indexOf(DIVIDER) + DIVIDER.length());
        if (StringUtils.isNumeric(actionIdString))
        {
            return Integer.parseInt(actionIdString);
        }
        return -1;
    }
}
