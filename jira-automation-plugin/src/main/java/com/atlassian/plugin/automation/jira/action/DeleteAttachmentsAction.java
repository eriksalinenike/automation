package com.atlassian.plugin.automation.jira.action;

import com.atlassian.fugue.Either;
import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.bc.issue.comment.CommentService;
import com.atlassian.jira.exception.RemoveException;
import com.atlassian.jira.issue.AttachmentManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.plugin.automation.core.Action;
import com.atlassian.plugin.automation.core.action.ActionConfiguration;
import com.atlassian.plugin.automation.core.auditlog.AuditString;
import com.atlassian.plugin.automation.core.auditlog.DefaultAuditString;
import com.atlassian.plugin.automation.jira.util.ErrorCollectionUtil;
import com.atlassian.plugin.automation.jira.util.SecurityLevelContextProvider;
import com.atlassian.plugin.automation.util.ErrorCollection;
import com.atlassian.plugin.automation.util.ParameterUtil;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Map;

import static com.atlassian.plugin.automation.jira.util.Constants.CONFIG_COMPLETE_KEY;
import static com.atlassian.plugin.automation.util.ParameterUtil.singleValue;

/**
 * Deletes attachments from the issue
 */
@Scanned
public class DeleteAttachmentsAction implements Action<Issue>
{
    public static final String COMMENT_SECURITY_KEY = "jiraCommentSecurityLevel";
    public static final String IGNORE_ATTACHMENTS_KEY = "jiraIgnoreAttachments";

    private static final Logger log = Logger.getLogger(DeleteAttachmentsAction.class);
    private static final String SEPARATOR_CHAR = ",";

    private final SoyTemplateRenderer soyTemplateRenderer;
    private final UserManager userManager;
    private final AttachmentManager attachmentManager;
    private final CommentService commentService;
    private final IssueService issueService;
    private final SecurityLevelContextProvider securityLevelContextProvider;
    private Either<String, Long> securityLevel;
    private List<String> ignoredAttachments;

    public DeleteAttachmentsAction(@ComponentImport final SoyTemplateRenderer soyTemplateRenderer,
                                   @ComponentImport final UserManager userManager,
                                   @ComponentImport final AttachmentManager attachmentManager,
                                   @ComponentImport final CommentService commentService,
                                   @ComponentImport final IssueService issueService,
                                   final SecurityLevelContextProvider securityLevelContextProvider)
    {
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.userManager = userManager;
        this.attachmentManager = attachmentManager;
        this.commentService = commentService;
        this.issueService = issueService;
        this.securityLevelContextProvider = securityLevelContextProvider;
    }

    @Override
    public void init(ActionConfiguration config)
    {
        securityLevel = securityLevelContextProvider.extractValue(singleValue(config, COMMENT_SECURITY_KEY));
        ignoredAttachments = Lists.newArrayList(StringUtils.split(singleValue(config, IGNORE_ATTACHMENTS_KEY), SEPARATOR_CHAR));
    }

    @Override
    public void execute(String actor, Iterable<Issue> items, ErrorCollection errorCollection)
    {
        final ApplicationUser actorUser = userManager.getUserByName(actor);
        List<String> deletedAttachmentsNames = Lists.newArrayList();


        log.debug("Deleting attachments");

        for (Issue issue : items)
        {
            for (Attachment attachment : issue.getAttachments())
            {
                final String attachmentName = attachment.getFilename();
                final boolean shouldIgnoreAttachment = Iterables.any(ignoredAttachments, new Predicate<String>()
                {
                    @Override
                    public boolean apply(@Nullable String wildcard)
                    {
                        return FilenameUtils.wildcardMatch(attachmentName, wildcard);
                    }
                });
                if (shouldIgnoreAttachment)
                {
                    log.debug(String.format("Attachment %s from issue %s does not need to be deleted.", attachmentName, issue.getKey()));
                }
                else
                {
                    try
                    {
                        attachmentManager.deleteAttachment(attachment);
                        deletedAttachmentsNames.add(attachmentName);
                    }
                    catch (RemoveException e)
                    {
                        log.error(String.format("Attachment %s from issue %s could not be deleted.", attachment.getFilename(), issue.getKey()));
                        errorCollection.addErrorMessage(e.getMessage());
                    }
                }
            }


            // If attachments have been deleted, the list of the names is not empty and we use them to add the comment.
            if (!deletedAttachmentsNames.isEmpty())
            {
                log.debug(String.format("Processing the issue: %s", issue.getKey()));
                final Issue mutableIssue = issueService.getIssue(actorUser, issue.getId()).getIssue();
                com.atlassian.jira.util.ErrorCollection jiraErrorCollection = new SimpleErrorCollection();
                final String comment = createComment(deletedAttachmentsNames);

                if (securityLevel != null)
                {
                    if (securityLevel.isLeft())
                    {
                        commentService.create(actorUser, mutableIssue, comment, securityLevel.left().get(), null,
                                true, jiraErrorCollection);
                    }
                    else if (securityLevel.isRight())
                    {
                        commentService.create(actorUser, mutableIssue, comment, null, securityLevel.right().get(),
                                true, jiraErrorCollection);
                    }
                }
                else
                {
                    commentService.create(actorUser, mutableIssue, comment, true, jiraErrorCollection);
                }

                if (jiraErrorCollection.hasAnyErrors())
                {
                    log.error(String.format("Unable to add comment on issue '%s' using actor '%s': %s", issue.getKey(), actor, jiraErrorCollection));
                    errorCollection.addErrorCollection(ErrorCollectionUtil.transform(jiraErrorCollection));
                }
                deletedAttachmentsNames.clear();
            }


            // We should try to delete the attachment directory as well, since we (hopefully) have no attachments there
            if (issue.getAttachments().isEmpty())
            {
                try
                {
                    attachmentManager.deleteAttachmentDirectory(issue);
                }
                catch (RemoveException e)
                {
                    log.error("Could not delete directory of issue: " + issue.getKey(), e);
                    errorCollection.addErrorMessage("Could not delete directory of issue: " + issue.getKey());
                }
            }
        }

        log.debug("Finished deleting attachments.");
    }

    @Override
    public AuditString getAuditLog()
    {
        return new DefaultAuditString(String.format("Deleted Attachments for matching issues"));
    }

    @Override
    public String getConfigurationTemplate(ActionConfiguration actionConfiguration, String actor)
    {
        try
        {
            final Map<String, Object> context = Maps.newHashMap();
            ParameterUtil.transformParams(context, actionConfiguration);
            String currentSecurityLevel = null;
            if (actionConfiguration != null)
            {
                currentSecurityLevel = singleValue(actionConfiguration, COMMENT_SECURITY_KEY);
            }

            final Map<String, Object> securityLevelContext = securityLevelContextProvider.getContext(userManager.getUserByName(actor), null, currentSecurityLevel);
            context.put("securityLevel", securityLevelContext);

            return soyTemplateRenderer.render(CONFIG_COMPLETE_KEY, "Atlassian.Templates.Automation.JIRA.deleteAttachmentsAction", context);
        }
        catch (SoyException e)
        {
            log.error("Error rendering template", e);
            return "Unable to render configuration form. Consult your server logs or administrator.";
        }
    }

    @Override
    public String getViewTemplate(ActionConfiguration actionConfiguration, String actor)
    {
        try
        {
            final Map<String, Object> context = Maps.newHashMap();
            String currentSecurityLevel = null;
            ParameterUtil.transformParams(context, actionConfiguration);
            if (actionConfiguration != null)
            {
                currentSecurityLevel = singleValue(actionConfiguration, COMMENT_SECURITY_KEY);
            }

            final Map<String, Object> securityLevelContext = securityLevelContextProvider.getContext(userManager.getUserByName(actor), null, currentSecurityLevel);
            context.put("securityLevel", securityLevelContext);

            return soyTemplateRenderer.render(CONFIG_COMPLETE_KEY, "Atlassian.Templates.Automation.JIRA.deleteAttachmentsActionView", context);
        }
        catch (SoyException e)
        {
            log.error("Error rendering template", e);
            return "Unable to render configuration form. Consult your server logs or administrator.";
        }
    }

    @Override
    public ErrorCollection validateAddConfiguration(I18nResolver i18n, Map<String, List<String>> params, String actor)
    {
        final ErrorCollection errorCollection = new ErrorCollection();
        final String securityKey = singleValue(params, COMMENT_SECURITY_KEY);

        if (StringUtils.isNotBlank(securityKey))
        {
            errorCollection.addErrorCollection(securityLevelContextProvider.validate(i18n,
                    userManager.getUser(actor), null, securityKey));
        }
        return errorCollection;
    }

    private String createComment(List<String> names)
    {
        final StringBuilder comment = new StringBuilder("The attachments of this issue have been deleted. The attachment names were:\n");
        for (String name : names)
        {
            comment.append("* ").append(name).append("\n");
        }
        return comment.toString();
    }
}
