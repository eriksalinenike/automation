package com.atlassian.plugin.automation.jira.util;

/**
* @author: mkonecny
*/
public final class RenderableEvent
{
    private final Long id;
    private final String name;
    private final boolean selected;

    public RenderableEvent(final Long id, final String name, final boolean selected)
    {
        this.id = id;
        this.name = name;
        this.selected = selected;
    }

    public Long getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public boolean isSelected()
    {
        return selected;
    }
}
