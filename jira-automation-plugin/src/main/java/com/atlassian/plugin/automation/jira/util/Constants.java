package com.atlassian.plugin.automation.jira.util;

/**
 * Defines some useful plugin constants
 */
public final class Constants
{
    public static final String PLUGIN_KEY = "com.atlassian.plugin.automation.jira-automation-plugin";
    public static final String CONFIG_RESOURCE_KEY = "jira-config-resources";

    public static final String CONFIG_COMPLETE_KEY = PLUGIN_KEY + ":" + CONFIG_RESOURCE_KEY;


    /**
     * Build number for JIRA 6.2
     */
    public static final int JIRA_62_BUILD_NUMBER = 6252;
}
