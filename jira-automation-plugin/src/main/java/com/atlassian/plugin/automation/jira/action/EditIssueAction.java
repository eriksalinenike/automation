package com.atlassian.plugin.automation.jira.action;

import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueInputParametersImpl;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.plugin.automation.core.Action;
import com.atlassian.plugin.automation.core.action.ActionConfiguration;
import com.atlassian.plugin.automation.core.auditlog.AuditString;
import com.atlassian.plugin.automation.core.auditlog.DefaultAuditString;
import com.atlassian.plugin.automation.jira.util.Constants;
import com.atlassian.plugin.automation.jira.util.CustomFieldValueRetriever;
import com.atlassian.plugin.automation.jira.util.ErrorCollectionUtil;
import com.atlassian.plugin.automation.jira.util.ParameterParserUtil;
import com.atlassian.plugin.automation.util.ErrorCollection;
import com.atlassian.plugin.automation.util.ParameterUtil;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.templaterenderer.RenderingException;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.atlassian.plugin.automation.util.ParameterUtil.singleValue;

@Scanned
public class EditIssueAction implements Action<Issue>
{
    public static final String EDIT_FIELDS_KEY = "jiraEditFields";
    public static final String EDIT_NOTIFICATION_KEY = "jiraEditNotification";
    public static final String ALLOW_VARIABLE_EXPANSION_KEY = "jiraAllowVariableExpansion";
    //
    private static final Logger log = Logger.getLogger(EditIssueAction.class);
    //
    private final UserManager userManager;
    private final IssueService issueService;
    private final SoyTemplateRenderer soyTemplateRenderer;
    private final TemplateRenderer templateRenderer;
    private final CustomFieldManager customFieldManager;
    private Map<String, String[]> fieldsMap;
    private boolean sendNotification;
    private boolean allowVariableExpansion;

    @Inject
    public EditIssueAction(
            @ComponentImport final UserManager userManager,
            @ComponentImport final IssueService issueService,
            @ComponentImport final SoyTemplateRenderer soyTemplateRenderer,
            @ComponentImport final TemplateRenderer templateRenderer,
            @ComponentImport final CustomFieldManager customFieldManager
    )
    {
        this.userManager = userManager;
        this.issueService = issueService;
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.templateRenderer = templateRenderer;
        this.customFieldManager = customFieldManager;
    }

    @Override
    public void init(ActionConfiguration config)
    {
        fieldsMap = Maps.newHashMap();
        final String fieldsText = singleValue(config, EDIT_FIELDS_KEY);
        sendNotification = Boolean.parseBoolean(singleValue(config, (EDIT_NOTIFICATION_KEY)));
        allowVariableExpansion = Boolean.parseBoolean(singleValue(config, (ALLOW_VARIABLE_EXPANSION_KEY)));
        if (!StringUtils.isBlank(fieldsText))
        {
            fieldsMap = ParameterParserUtil.getFieldsMap(fieldsText);
        }
    }

    @Override
    public void execute(String actor, Iterable<Issue> items, ErrorCollection errorCollection)
    {

        log.debug("Processing issues");
        final ApplicationUser user = userManager.getUserByName(actor);
        for (Issue issue : items)
        {
            log.debug("Processing the issue: " + issue.getKey());
            final Issue mutableIssue = issueService.getIssue(user, issue.getId()).getIssue();
            com.atlassian.jira.util.ErrorCollection jiraErrorCollection = new SimpleErrorCollection();

            Map<String, String[]> renderedFieldsMap;

            if (allowVariableExpansion)
            {
                renderedFieldsMap = renderFields(issue);
            }
            else
            {
                renderedFieldsMap = fieldsMap;
            }

            final IssueInputParametersImpl params = new IssueInputParametersImpl(renderedFieldsMap);
            params.setRetainExistingValuesWhenParameterNotProvided(true, true);
            params.setSkipScreenCheck(true);

            IssueService.UpdateValidationResult validationResult = issueService.validateUpdate(user, mutableIssue.getId(), params);

            if (validationResult.isValid())
            {
                final IssueService.IssueResult updateResult = issueService.update(user, validationResult, EventDispatchOption.ISSUE_UPDATED, sendNotification);
                if (!updateResult.isValid())
                {
                    jiraErrorCollection.addErrorCollection(updateResult.getErrorCollection());

                }
            }
            else
            {
                jiraErrorCollection.addErrorCollection(validationResult.getErrorCollection());
            }
            if (jiraErrorCollection.hasAnyErrors())
            {
                log.error(String.format("Unable to edit issue '%s' using actor '%s': %s", issue.getKey(), actor, jiraErrorCollection));
                errorCollection.addErrorCollection(ErrorCollectionUtil.transform(jiraErrorCollection));
            }
        }
    }

    @Override
    public AuditString getAuditLog()
    {
        return new DefaultAuditString(String.format("Edit issue - Fields set: '%s'", ParameterParserUtil.getFieldsMapString(this.fieldsMap)));
    }

    @Override
    public String getConfigurationTemplate(ActionConfiguration actionConfiguration, String actor)
    {
        try
        {
            final Map<String, Object> context = Maps.newHashMap();
            ParameterUtil.transformParams(context, actionConfiguration);

            return soyTemplateRenderer.render(Constants.CONFIG_COMPLETE_KEY, "Atlassian.Templates.Automation.JIRA.editAction", context);
        }
        catch (SoyException e)
        {
            log.error("Error rendering template", e);
            return "Unable to render configuration form. Consult your server logs or administrator.";
        }
    }

    @Override
    public String getViewTemplate(final ActionConfiguration actionConfiguration, final String actor)
    {
        try
        {
            final Map<String, Object> context = Maps.newHashMap();
            ParameterUtil.transformParams(context, actionConfiguration);

            return soyTemplateRenderer.render(Constants.CONFIG_COMPLETE_KEY, "Atlassian.Templates.Automation.JIRA.editActionView", context);
        }
        catch (SoyException e)
        {
            log.error("Error rendering template", e);
            return "Unable to render configuration form. Consult your server logs or administrator.";
        }
    }

    @Override
    public ErrorCollection validateAddConfiguration(I18nResolver i18n, Map<String, List<String>> params, String actor)
    {
        final ErrorCollection errors = new ErrorCollection();
        if (!params.containsKey(EDIT_FIELDS_KEY) || StringUtils.isBlank(singleValue(params, EDIT_FIELDS_KEY)))
        {
            errors.addError(EDIT_FIELDS_KEY, i18n.getText("automation.jira.jiraEditFields.empty"));
            return errors;
        }
        // try to parse the fields
        if (ParameterParserUtil.getFieldsMap(singleValue(params, EDIT_FIELDS_KEY)) == null)
        {
            errors.addError(EDIT_FIELDS_KEY, i18n.getText("automation.jira.jiraEditFields.invalid"));
        }
        return errors;
    }

    protected Map<String, String[]> renderFields(Issue issue)
    {

        Map<String, String[]> renderedFieldsMap = Maps.newHashMap();

        final Map<String, Object> context = Maps.newHashMap();
        context.put("issue", issue);
        context.put("customfields", new CustomFieldValueRetriever(customFieldManager, issue));

        for (Map.Entry<String, String[]> field : fieldsMap.entrySet())
        {

            String[] values = field.getValue();
            List<String> renderedValues = new ArrayList<String>();

            for (String value : values)
            {
                try
                {
                    String renderedField = templateRenderer.renderFragment(value, context);
                    renderedValues.add(renderedField);
                }
                catch (RenderingException e)
                {
                    renderedValues.add(value);
                }
            }

            renderedFieldsMap.put(field.getKey(), renderedValues.toArray(new String[renderedValues.size()]));

        }

        return renderedFieldsMap;
    }
}
