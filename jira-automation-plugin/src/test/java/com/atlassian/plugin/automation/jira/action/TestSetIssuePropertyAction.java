package com.atlassian.plugin.automation.jira.action;

import com.atlassian.jira.bc.issue.properties.IssuePropertyService;
import com.atlassian.jira.entity.property.EntityPropertyService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.plugin.automation.config.DefaultAutomationConfiguration;
import com.atlassian.plugin.automation.jira.util.Constants;
import com.atlassian.plugin.automation.util.ErrorCollection;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;
import java.util.Map;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestSetIssuePropertyAction extends TestCase
{
    @Mock
    private UserManager userManager;
    @Mock
    private SoyTemplateRenderer soyTemplateRenderer;
    @Mock
    private I18nResolver i18nResolver;
    @Mock
    private BuildUtilsInfo buildUtilsInfo;
    @Mock
    private MutableIssue issue;
    @Mock
    private IssuePropertyService issuePropertyService;

    private SimpleErrorCollection validationErrorCollection;
    private SetIssuePropertyAction setIssuePropertyAction;
    private Map<String, List<String>> params;
    private EntityPropertyService.SetPropertyValidationResult setPropertyValidationResult;
    private EntityPropertyService.PropertyResult propertyResult;

    @Before
    public void setUp() throws Exception
    {
        when(i18nResolver.getText(Matchers.anyString())).thenReturn("translated text");
        when(buildUtilsInfo.getApplicationBuildNumber()).thenReturn(Constants.JIRA_62_BUILD_NUMBER);
        when(issue.getId()).thenReturn(1000l);

        validationErrorCollection = new SimpleErrorCollection();
        when(issuePropertyService.validatePropertyInput(any(EntityPropertyService.PropertyInput.class))).thenReturn(validationErrorCollection);
        setIssuePropertyAction = new SetIssuePropertyAction(userManager, soyTemplateRenderer, issuePropertyService, buildUtilsInfo);

        params = Maps.newHashMap();
        setPropertyValidationResult = mock(EntityPropertyService.SetPropertyValidationResult.class);
        when(issuePropertyService.validateSetProperty(any(ApplicationUser.class), anyLong(), any(EntityPropertyService.PropertyInput.class))).thenReturn(setPropertyValidationResult);

        propertyResult = mock(EntityPropertyService.PropertyResult.class);
        when(issuePropertyService.setProperty(any(ApplicationUser.class), eq(setPropertyValidationResult))).thenReturn(propertyResult);
    }

    @Test
    public void testExecutePasses() throws Exception
    {
        params.put(SetIssuePropertyAction.SET_PROPERTY_FIELD_KEY, Lists.newArrayList("foo"));
        params.put(SetIssuePropertyAction.SET_PROPERTY_VALUE_KEY, Lists.newArrayList("true"));
        setIssuePropertyAction.init(new DefaultAutomationConfiguration(-1, "", params));

        when(setPropertyValidationResult.isValid()).thenReturn(true);
        when(propertyResult.isValid()).thenReturn(true);

        ErrorCollection errorCollection = new ErrorCollection();
        setIssuePropertyAction.execute("admin", Lists.newArrayList((Issue) issue), errorCollection);

        assertFalse("errors present", errorCollection.hasAnyErrors());
    }

    @Test
    public void testExecuteFails() throws Exception
    {
        params.put(SetIssuePropertyAction.SET_PROPERTY_FIELD_KEY, Lists.newArrayList("foo"));
        params.put(SetIssuePropertyAction.SET_PROPERTY_VALUE_KEY, Lists.newArrayList("true"));
        setIssuePropertyAction.init(new DefaultAutomationConfiguration(-1, "", params));

        validationErrorCollection.addErrorMessage("error");
        when(setPropertyValidationResult.isValid()).thenReturn(false);
        when(setPropertyValidationResult.getErrorCollection()).thenReturn(validationErrorCollection);

        ErrorCollection errorCollection = new ErrorCollection();
        setIssuePropertyAction.execute("admin", Lists.newArrayList((Issue) issue), errorCollection);

        assertTrue("no errors present", errorCollection.hasAnyErrors());
    }

    @Test
    public void testExecuteFailsAfterValidation() throws Exception
    {
        params.put(SetIssuePropertyAction.SET_PROPERTY_FIELD_KEY, Lists.newArrayList("foo"));
        params.put(SetIssuePropertyAction.SET_PROPERTY_VALUE_KEY, Lists.newArrayList("true"));
        setIssuePropertyAction.init(new DefaultAutomationConfiguration(-1, "", params));

        validationErrorCollection.addErrorMessage("error");
        when(setPropertyValidationResult.isValid()).thenReturn(true);

        when(propertyResult.isValid()).thenReturn(false);
        when(propertyResult.getErrorCollection()).thenReturn(validationErrorCollection);

        ErrorCollection errorCollection = new ErrorCollection();
        setIssuePropertyAction.execute("admin", Lists.newArrayList((Issue) issue), errorCollection);

        assertTrue("no errors present", errorCollection.hasAnyErrors());
    }

    @Test
    public void testValidateAddConfiguration() throws Exception
    {
        assertTrue("no error produced", setIssuePropertyAction.validateAddConfiguration(i18nResolver, params, "").hasAnyErrors());

        params.put(SetIssuePropertyAction.SET_PROPERTY_FIELD_KEY, Lists.newArrayList("foo"));
        assertTrue("no error produced", setIssuePropertyAction.validateAddConfiguration(i18nResolver, params, "").hasAnyErrors());

        params.put(SetIssuePropertyAction.SET_PROPERTY_VALUE_KEY, Lists.newArrayList("true"));
        assertFalse("error produced", setIssuePropertyAction.validateAddConfiguration(i18nResolver, params, "").hasAnyErrors());

        validationErrorCollection.addErrorMessage("error");
        assertTrue("error produced", setIssuePropertyAction.validateAddConfiguration(i18nResolver, params, "").hasAnyErrors());
    }
}
