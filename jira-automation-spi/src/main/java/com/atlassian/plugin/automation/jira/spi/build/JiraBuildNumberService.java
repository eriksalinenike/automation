package com.atlassian.plugin.automation.jira.spi.build;

import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.plugin.automation.spi.build.BuildNumberService;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import javax.inject.Inject;
import javax.inject.Named;

@Named
@ExportAsService
public class JiraBuildNumberService implements BuildNumberService
{
    private final int applicationBuildNumber;

    @Inject
    public JiraBuildNumberService(@ComponentImport final BuildUtilsInfo buildUtilsInfo)
    {
        applicationBuildNumber = buildUtilsInfo.getApplicationBuildNumber();
    }

    @Override
    public long getBuildNumber()
    {
        return applicationBuildNumber;
    }
}
