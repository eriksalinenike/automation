package com.atlassian.plugin.automation.jira.spi.scheduler;

import com.atlassian.sal.api.scheduling.PluginJob;
import com.atlassian.scheduler.JobRunner;
import com.atlassian.scheduler.JobRunnerRequest;
import com.atlassian.scheduler.JobRunnerResponse;
import org.apache.log4j.Logger;

import javax.annotation.Nullable;
import java.io.Serializable;
import java.util.Map;

/**
 * Provides wrapper for the new JIRA 6.3+ Scheduler interface
 */
public class JiraJobRunnerWrapper implements JobRunner
{
    public static final String JOB_CLASS_CANONICAL_NAME = "pluginJobClassName";

    private static final Logger log = Logger.getLogger(JiraJobRunnerWrapper.class);
    private final Map<String, Object> jobDataMap;
    private final ClassLoader classLoader;

    public JiraJobRunnerWrapper(final Map<String, Object> jobDataMap, final ClassLoader classLoader)
    {
        this.jobDataMap = jobDataMap;
        this.classLoader = classLoader;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    @Override
    public JobRunnerResponse runJob(final JobRunnerRequest request)
    {
        final Map<String, Serializable> params = request.getJobConfig().getParameters();
        try
        {
            final Class<? extends PluginJob> jobClass = (Class<? extends PluginJob>) classLoader.loadClass((String) params.get(JOB_CLASS_CANONICAL_NAME));
            jobClass.newInstance().execute(jobDataMap);
            return JobRunnerResponse.success();
        }
        catch (Exception e)
        {
            log.error(String.format("Unable to execute the job [id:%s]", request.getJobId().toString()), e);
            return JobRunnerResponse.failed(e);
        }
    }
}
