package com.atlassian.plugin.automation.page;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.plugin.automation.page.util.RuleFormUtils;

import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

public class ConfirmationForm
{
    @Inject
    private PageBinder pageBinder;

    @ElementBy(id = "save-rule-form")
    private PageElement saveButton;
    
    @ElementBy(id = "sync-enabled")
    private PageElement syncEnabled;

    @ElementBy(id = "async-enabled")
    private PageElement asyncEnabled;

    @WaitUntil
    public void isReady()
    {
        waitUntilTrue(saveButton.timed().isPresent());
    }

    public AdminPage save()
    {
        RuleFormUtils.waitForFormRerender();
        saveButton.click();
        return pageBinder.bind(AdminPage.class);
    }
    
    public boolean isIssueEventSyncProcessingEnabled()
    {
        return syncEnabled != null && syncEnabled.isVisible();
    }

    public boolean isIssueEventAsyncProcessingEnabled()
    {
        return asyncEnabled != null && asyncEnabled.isVisible();
    }
}
