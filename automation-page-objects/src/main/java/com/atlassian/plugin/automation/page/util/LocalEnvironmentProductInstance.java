package com.atlassian.plugin.automation.page.util;

import com.atlassian.core.util.ClassLoaderUtils;
import com.atlassian.jira.testkit.client.log.FuncTestOut;
import com.atlassian.pageobjects.ProductInstance;
import org.apache.commons.io.IOUtils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class LocalEnvironmentProductInstance implements ProductInstance
{
    private static final String TEST_SERVER_PROPERTIES = "test.server.properties";
    private static final String DEFAULT_PROPERTIES_FILENAME = "localtest.properties";

    private static final String HOST = "confluence.host";
    private static final String PROTOCOL = "confluence.protocol";
    private static final String PORT = "confluence.port";
    private static final String CONTEXT = "confluence.context";
    private static final String INSTANCE = "confluence.instanceId";

    private final String baseUrl;
    private final String protocol;
    private int httpPort;
    private final String contextPath;
    private final String instanceId;

    public LocalEnvironmentProductInstance()
    {
        this(loadProperties(TEST_SERVER_PROPERTIES, DEFAULT_PROPERTIES_FILENAME));
    }

    public LocalEnvironmentProductInstance(final Properties properties)
    {
        this.baseUrl = properties.getProperty(HOST, "localhost");
        this.contextPath = properties.getProperty(CONTEXT, "/confluence");
        this.instanceId = properties.getProperty(INSTANCE, "confluence");
        this.protocol = properties.getProperty(PROTOCOL, "http");
        try
        {
            this.httpPort = Integer.parseInt(properties.getProperty(PORT, "1990"));
        }
        catch (NumberFormatException e)
        {
            this.httpPort = 1990;
        }

        //set some system properties which are used by Confluence's RPC client.
        System.setProperty("baseurl.confluence", getBaseUrl());
        System.setProperty("http.confluence.port", String.valueOf(getHttpPort()));
        System.setProperty("context.confluence.path", getContextPath());
    }

    @Override
    public String getBaseUrl()
    {
        return protocol + "://" + this.baseUrl + ":" + this.httpPort + contextPath;
    }

    @Override
    public int getHttpPort()
    {
        return this.httpPort;
    }

    @Override
    public String getContextPath()
    {
        return this.contextPath;
    }

    @Override
    public String getInstanceId()
    {
        return this.instanceId;
    }

    public static Properties loadProperties(String key, String def)
    {
        Properties properties = new Properties();
        String propertiesFileName = "";
        try
        {
            propertiesFileName = System.getProperty(key, def);

            InputStream propStream = ClassLoaderUtils.getResourceAsStream(propertiesFileName, LocalEnvironmentProductInstance.class);
            if (propStream == null)
            {
                // The resource was not found on the classpath. Try opening as a file
                propStream = new FileInputStream(propertiesFileName);
            }
            try
            {
                properties.load(propStream);
                return properties;
            }
            finally
            {
                IOUtils.closeQuietly(propStream);
            }
        }
        catch (IOException e)
        {
            FuncTestOut.out.println("Cannot load file " + propertiesFileName + " from CLASSPATH.");
            e.printStackTrace(FuncTestOut.out);
            throw new IllegalArgumentException("Could not load properties file " + propertiesFileName + " from classpath");
        }
    }
}
