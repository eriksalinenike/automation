package com.atlassian.plugin.automation.page.trigger;


import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.plugin.automation.page.ModuleKey;

@ModuleKey ("com.atlassian.plugin.automation.confluence-automation-plugin:search-trigger")
public class ConfluenceSearchTriggerForm extends TriggerForm
{
    public ConfluenceSearchTriggerForm(final PageElement container, final String moduleKey)
    {
        super(container, moduleKey);
    }

    public ConfluenceSearchTriggerForm setCron(final String cron)
    {
        setTriggerParam("cronExpression", cron);
        return this;
    }

    public ConfluenceSearchTriggerForm setQuery(final String query)
    {
        setTriggerParam("query", query);
        return this;
    }

    public ConfluenceSearchTriggerForm setSpaceKeys(final String spaceKeys)
    {
        setTriggerParam("spaceKeys", spaceKeys);
        return this;
    }
}


