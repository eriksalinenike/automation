package com.atlassian.plugin.automation.page;

import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import com.atlassian.webdriver.AtlassianWebDriver;

import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

/**
 * @author: mkonecny
 */
public class ImportRuleFragment
{
    @Inject
    private AtlassianWebDriver driver;

    @Inject
    private PageElementFinder elementFinder;

    @ElementBy(id = "import-rule")
    private PageElement importRuleElement;

    @ElementBy(id = "importErrorInlineDialog")
    private PageElement importErrorContainer;

    @WaitUntil
    public void isReady()
    {
        waitUntilTrue(importRuleElement.timed().isPresent());
    }

    public void importRule(String filename)
    {
        // small hack to show the import rule file input to successfully upload the file
        driver.executeScript("AJS.$(\"#import-rule\").show();");
        importRuleElement.type(filename);
    }

    public TimedQuery<Boolean> importFailed()
    {
        return importErrorContainer.timed().isPresent();
    }
}
