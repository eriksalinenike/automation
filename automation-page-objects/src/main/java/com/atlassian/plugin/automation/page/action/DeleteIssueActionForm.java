package com.atlassian.plugin.automation.page.action;

import com.atlassian.pageobjects.elements.CheckboxElement;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.plugin.automation.page.ModuleKey;
import org.openqa.selenium.By;

/**
 * @author: mkonecny
 */
@ModuleKey("com.atlassian.plugin.automation.jira-automation-plugin:delete-issue-action")
public class DeleteIssueActionForm extends ActionForm
{
    public DeleteIssueActionForm(PageElement container, String moduleKey)
    {
        super(container, moduleKey);
    }


    public DeleteIssueActionForm dispatchEvent(final boolean shouldDispatch)
    {

        final CheckboxElement checkbox = container.find(By.name("jiraDeleteIssueNotification"), CheckboxElement.class);
        if (shouldDispatch)
        {
            checkbox.check();
        }
        else
        {
            checkbox.uncheck();
        }
        return this;
    }

}
