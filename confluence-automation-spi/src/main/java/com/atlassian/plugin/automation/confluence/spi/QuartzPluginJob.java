package com.atlassian.plugin.automation.confluence.spi;

import com.atlassian.sal.api.scheduling.PluginJob;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * A Quartz job that executes a PluginJob
 */
public class QuartzPluginJob implements Job
{
    private static final Logger log = LoggerFactory.getLogger(QuartzPluginJob.class);

    public static final String JOB_CLASS_KEY = "pluginJobClass";
    public static final String JOB_DATA_MAP_KEY = "pluginJobDataMap";

    public void execute(final JobExecutionContext jobExecutionContext) throws JobExecutionException
    {
        final JobDataMap map = jobExecutionContext.getJobDetail().getJobDataMap();
        @SuppressWarnings ("unchecked")
        final Class<? extends PluginJob> jobClass = (Class<? extends PluginJob>) map.get(JOB_CLASS_KEY);
        @SuppressWarnings ("unchecked")
        final Map<String, Object> pluginJobMap = (Map<String, Object>) map.get(JOB_DATA_MAP_KEY);

        // Instantiate the job
        PluginJob job;
        try
        {
            job = jobClass.newInstance();
        }
        catch (final InstantiationException ie)
        {
            throw new JobExecutionException("Error instantiating job", ie, false);
        }
        catch (final IllegalAccessException iae)
        {
            throw new JobExecutionException("Cannot access job class", iae, false);
        }

        try
        {
            job.execute(pluginJobMap);
        }
        catch (RuntimeException e)
        {
            log.error("Runtime error occurred", e);
            throw new JobExecutionException(e, true);
        }
        catch (Throwable t)
        {
            log.error("Undeclared error was thrown", t);
            throw new JobExecutionException("Undeclared error was thrown");
        }
    }
}