package com.atlassian.plugin.automation.confluence.trigger;

import com.atlassian.confluence.core.DateFormatter;
import com.atlassian.confluence.core.FormatSettingsManager;
import com.atlassian.confluence.core.TimeZone;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.search.service.ContentTypeEnum;
import com.atlassian.confluence.search.service.PredefinedSearchBuilder;
import com.atlassian.confluence.search.service.SearchQueryParameters;
import com.atlassian.confluence.search.v2.InvalidSearchException;
import com.atlassian.confluence.search.v2.SearchManager;
import com.atlassian.confluence.search.v2.SearchResult;
import com.atlassian.plugin.automation.confluence.spi.ConfluenceCronExpressionValidator;
import com.atlassian.plugin.automation.core.AbstractCronTrigger;
import com.atlassian.plugin.automation.core.TriggerContext;
import com.atlassian.plugin.automation.core.auditlog.AuditString;
import com.atlassian.plugin.automation.core.auditlog.DefaultAuditString;
import com.atlassian.plugin.automation.core.trigger.TriggerConfiguration;
import com.atlassian.plugin.automation.util.ErrorCollection;
import com.atlassian.plugin.automation.util.ParameterUtil;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.atlassian.plugin.automation.util.ParameterUtil.singleValue;

/**
 * Runs a basic Confluence search using a simple query param.
 */
public class SearchTrigger extends AbstractCronTrigger<SearchResult>
{
    private static final Logger log = Logger.getLogger(SearchTrigger.class);
    private static final String RESOURCE_KEY = "com.atlassian.plugin.automation.confluence-automation-plugin:confluence-config-resources";

    private static final String SPACE_KEYS = "spaceKeys";
    private static final String QUERY_KEY = "query";
    private static final String MAX_RESULTS_KEY = "maxResults";

    private final SearchManager searchManager;
    private final PredefinedSearchBuilder predefinedSearchBuilder;
    private final SoyTemplateRenderer soyTemplateRenderer;
    private final FormatSettingsManager formatSettingsManager;
    private final LocaleManager localeManager;

    private String query;
    private int maxResults;
    private Set<String> spaceKeys = Sets.newHashSet();

    public SearchTrigger(final SearchManager searchManager, final PredefinedSearchBuilder predefinedSearchBuilder,
            final SoyTemplateRenderer soyTemplateRenderer, final FormatSettingsManager formatSettingsManager,
            final LocaleManager localeManager)
    {
        this.searchManager = searchManager;
        this.predefinedSearchBuilder = predefinedSearchBuilder;
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.formatSettingsManager = formatSettingsManager;
        this.localeManager = localeManager;
    }

    @Override
    public void init(final TriggerConfiguration config)
    {
        super.init(config);
        query = singleValue(config, QUERY_KEY);
        String maxResultsParam = singleValue(config, MAX_RESULTS_KEY);
        String spaceKeysString = singleValue(config, SPACE_KEYS);
        if (StringUtils.isNotBlank(spaceKeysString))
        {
            spaceKeys.addAll(Arrays.asList(StringUtils.split(spaceKeysString, ",")));
        }

        if (StringUtils.isBlank(maxResultsParam))
        {
            maxResultsParam = "100";
        }
        maxResults = Integer.parseInt(maxResultsParam);
    }

    @Override
    public Iterable<SearchResult> getItems(final TriggerContext context, ErrorCollection errorCollection)
    {
        final SearchQueryParameters params = new SearchQueryParameters(query);
        params.setContentTypes(Sets.newHashSet(ContentTypeEnum.BLOG, ContentTypeEnum.PAGE));

        if (!spaceKeys.isEmpty())
        {
            params.setSpaceKeys(spaceKeys);
        }
        try
        {
            return searchManager.search(predefinedSearchBuilder.buildSiteSearch(params, 0, maxResults));
        }
        catch (InvalidSearchException e)
        {
            errorCollection.addErrorMessage("Error executing confluence search (" + convertParamsToString() + "): " + e.getMessage());
        }
        return Lists.newArrayList();
    }

    @Override
    public AuditString getAuditLog()
    {
        return new DefaultAuditString(String.format("Confluence search trigger (%s)", convertParamsToString()));
    }

    @Override
    public String getConfigurationTemplate(TriggerConfiguration triggerConfiguration, final String actor)
    {
        try
        {
            final Map<String, Object> context = Maps.newHashMap();
            ParameterUtil.transformParams(context, triggerConfiguration);
            final DateFormatter formatter = new DateFormatter(TimeZone.getDefault(), formatSettingsManager, localeManager);
            context.put("currentServerTime", formatter.format(new Date()));
            return soyTemplateRenderer.render(RESOURCE_KEY, "Atlassian.Templates.Automation.Confluence.searchTrigger", context);
        }
        catch (SoyException e)
        {
            log.error("Error rendering template", e);
            return "Unable to render configuration form. Consult your server logs or administrator.";
        }
    }

    @Override
    public String getViewTemplate(final TriggerConfiguration triggerConfiguration, final String actor)
    {
        try
        {
            final Map<String, Object> context = Maps.newHashMap();
            ParameterUtil.transformParams(context, triggerConfiguration);
            return soyTemplateRenderer.render(RESOURCE_KEY, "Atlassian.Templates.Automation.Confluence.searchTriggerView", context);
        }
        catch (SoyException e)
        {
            log.error("Error rendering template", e);
            return "Unable to render configuration form. Consult your server logs or administrator.";
        }
    }

    @Override
    public ErrorCollection validateAddConfiguration(I18nResolver i18n, Map<String, List<String>> params, String actor)
    {
        final ErrorCollection errorCollection = super.validateAddConfiguration(i18n, params, actor);
        if (StringUtils.isBlank(singleValue(params, QUERY_KEY)))
        {
            errorCollection.addError(QUERY_KEY, i18n.getText("automation.confluence.value.invalid"));
        }
        final String maxResults = singleValue(params, MAX_RESULTS_KEY);
        if (!StringUtils.isBlank(maxResults) && (!StringUtils.isNumeric(maxResults) || Integer.parseInt(maxResults) < 0))
        {
            errorCollection.addError(MAX_RESULTS_KEY, i18n.getText("automation.confluence.value.invalid"));
        }

        return errorCollection;
    }

    @Override
    protected boolean isValidCronExpression(String cronExpression)
    {
        return ConfluenceCronExpressionValidator.isValidExpression(cronExpression);
    }

    private String convertParamsToString()
    {
        if (spaceKeys.isEmpty())
        {
            return String.format("query='%s', All Spaces, maxResults=%d", query, maxResults);
        }
        else
        {
            return String.format("query='%s', spaces='%s', maxResults=%d", query, Joiner.on(",").skipNulls().join(spaceKeys), maxResults);
        }

    }
}
