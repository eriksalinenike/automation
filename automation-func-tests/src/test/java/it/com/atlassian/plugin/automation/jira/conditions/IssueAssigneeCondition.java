package it.com.atlassian.plugin.automation.jira.conditions;

import com.atlassian.jira.testkit.client.IssuesControl;
import com.atlassian.jira.testkit.client.restclient.User;
import com.atlassian.pageobjects.elements.query.AbstractTimedCondition;

public class IssueAssigneeCondition extends AbstractTimedCondition
{
    private static final int MAX_TIMEOUT = 15000;
    private static final int POLLING_INTERVAL = 500;
    private final IssuesControl issuesControl;
    private final String issueKey;
    private final String expectedAssignee;

    public IssueAssigneeCondition(final IssuesControl issuesControl, String issueKey, String expectedAssignee)
    {
        super(MAX_TIMEOUT, POLLING_INTERVAL);
        this.issuesControl = issuesControl;
        this.issueKey = issueKey;
        this.expectedAssignee = expectedAssignee;
    }

    @Override
    protected Boolean currentValue()
    {
        final User assigneeUser = issuesControl.getIssue(issueKey).fields.assignee;
        final String assigneeUserName = (assigneeUser != null) ? assigneeUser.name : null;
        return expectedAssignee.equals(assigneeUserName);
    }
}
