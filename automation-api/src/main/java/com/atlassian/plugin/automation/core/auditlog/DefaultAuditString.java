package com.atlassian.plugin.automation.core.auditlog;

public class DefaultAuditString implements AuditString
{
    private final String log;

    public DefaultAuditString(final String log)
    {
        this.log = log;
    }

    @Override
    public String getString()
    {
        return log;
    }
}
