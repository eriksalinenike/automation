package com.atlassian.plugin.automation.status;

import com.atlassian.cache.compat.Cache;
import com.atlassian.cache.compat.CacheFactory;
import com.atlassian.cache.compat.CacheLoader;
import com.atlassian.cache.compat.CacheSettingsBuilder;

import javax.inject.Inject;
import javax.inject.Named;

@Named
@SuppressWarnings("unused")
public class DefaultRuleStatusService implements RuleStatusService
{
    private final Cache<Integer, RuleStatus> ruleStatuses;

    @Inject
    public DefaultRuleStatusService(final CacheFactory cacheFactory)
    {
        // we should not need any max entries, as this is only going to contain as many entries as there are rules
        // local cache is okay as this gets called after each rule is executed and might lead to performance issues
        ruleStatuses = cacheFactory.getCache(this.getClass().getName(),
                new CacheLoader<Integer, RuleStatus>()
                {
                    @Override
                    public RuleStatus load(Integer key)
                    {
                        return RuleStatus.UNKNOWN;
                    }
                },
                new CacheSettingsBuilder()
                        .flushable()
                        .local()
                        .build());
    }

    @Override
    public void setRuleStatus(int ruleId, RuleStatus status)
    {
        ruleStatuses.put(ruleId, status);
    }

    @Override
    public RuleStatus getRuleStatus(int ruleId)
    {
        return ruleStatuses.get(ruleId);
    }
}
