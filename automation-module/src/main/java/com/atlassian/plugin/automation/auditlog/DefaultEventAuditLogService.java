package com.atlassian.plugin.automation.auditlog;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.fugue.Either;
import com.atlassian.plugin.automation.core.auditlog.AuditMessage;
import com.atlassian.plugin.automation.core.auditlog.EventAuditMessage;
import com.atlassian.plugin.automation.core.auditlog.EventAuditMessageBuilder;
import com.atlassian.plugin.automation.spi.auditlog.RuleLogService;
import com.atlassian.plugin.automation.status.RuleStatus;
import com.atlassian.plugin.automation.status.RuleStatusService;
import com.atlassian.plugin.automation.util.ErrorCollection;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.user.UserManager;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import net.java.ao.DBParam;
import net.java.ao.Query;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;

import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static com.atlassian.plugin.automation.config.ao.CurrentSchema.ActionMessage;
import static com.atlassian.plugin.automation.config.ao.CurrentSchema.AuditMessageEntity;

@Named
@ExportAsService
@SuppressWarnings("unused")
public class DefaultEventAuditLogService implements EventAuditLogService
{
    private static final Logger log = Logger.getLogger(DefaultEventAuditLogService.class);

    // Never return more than xxx results
    private static final int MAX_RESULTS = 1000;

    private final I18nResolver i18n;
    private final ActiveObjects ao;
    private final UserManager userManager;
    private final RuleStatusService ruleStatusService;
    private final RuleLogService ruleLogService;

    @Inject
    public DefaultEventAuditLogService(
            @ComponentImport final I18nResolver i18n,
            @ComponentImport final ActiveObjects ao,
            @ComponentImport final UserManager userManager,
            final RuleStatusService ruleStatusService,
            @ComponentImport final RuleLogService ruleLogService)
    {
        this.i18n = i18n;
        this.ao = ao;
        this.userManager = userManager;
        this.ruleStatusService = ruleStatusService;
        this.ruleLogService = ruleLogService;
    }

    public int getEntriesCount(String user)
    {
        if (StringUtils.isBlank(user) || !userManager.isAdmin(user))
        {
            return 0;
        }
        return ao.count(AuditMessageEntity.class);
    }

    @Override
    public Either<ErrorCollection, Iterable<AuditMessage>> getAllEntries(String user, int startAt, int maxResults)
    {
        final ErrorCollection errorCollection = new ErrorCollection();
        if (startAt < 0)
        {
            errorCollection.addErrorMessage(i18n.getText("automation.plugin.audit.log.invalid.start"), ErrorCollection.Reason.VALIDATION_FAILED);
        }

        if (maxResults < 0)
        {
            errorCollection.addErrorMessage(i18n.getText("automation.plugin.audit.log.invalid.max"), ErrorCollection.Reason.VALIDATION_FAILED);
        }

        if (StringUtils.isBlank(user) || !userManager.isAdmin(user))
        {
            errorCollection.addErrorMessage(i18n.getText("perm.denied.desc"), ErrorCollection.Reason.FORBIDDEN);
        }

        if (errorCollection.hasAnyErrors())
        {
            return Either.left(errorCollection);
        }
        int resultsLimit = Math.min(MAX_RESULTS, maxResults);
        List<AuditMessage> messages = Lists.newArrayList();

        for (AuditMessageEntity msgEntity : ao.find(AuditMessageEntity.class, Query.select().offset(startAt).limit(resultsLimit).order("DATE DESC")))
        {
            final Iterable<String> actionMessages = Iterables.transform(Arrays.asList(msgEntity.getActionMessages()), new Function<ActionMessage, String>()
            {
                @Override
                public String apply(@Nullable final ActionMessage from)
                {
                    if (from != null)
                    {
                        return from.getMessage();
                    }
                    return null;
                }
            });
            messages.add(new EventAuditMessageBuilder().
                    setTimestamp(msgEntity.getDate()).
                    setActor(msgEntity.getActor()).
                    setRuleId(msgEntity.getRuleId()).
                    setMessage(msgEntity.getMessage()).
                    setTriggerMessage(msgEntity.getTriggerMessage()).
                    setActionMessages(actionMessages).
                    setErrors(msgEntity.getErrors()).build());
        }
        return Either.right((Iterable<AuditMessage>) messages);
    }

    @Override
    public void addEntry(EventAuditMessage eventAuditMessage)
    {
        final AuditMessageEntity newAuditLog = ao.create(AuditMessageEntity.class, new DBParam("DATE", eventAuditMessage.getTimestamp()),
                new DBParam("ACTOR", eventAuditMessage.getActor()),
                new DBParam("ERRORS", eventAuditMessage.getErrors()),
                new DBParam("RULE_ID", eventAuditMessage.getRuleId()),
                new DBParam("MESSAGE", eventAuditMessage.getMessage()),
                new DBParam("TRIGGER_MESSAGE", eventAuditMessage.getTriggerMessage()));

        for (String actionMessage : eventAuditMessage.getActionMessages())
        {
            ao.create(ActionMessage.class, new DBParam("MESSAGE", actionMessage),
                    new DBParam("AUDIT_MESSAGE_ENTITY_ID", newAuditLog.getID()));
        }

        addStatusEntry(eventAuditMessage.getRuleId(), StringUtils.isBlank(eventAuditMessage.getErrors()) ? RuleStatus.OK : RuleStatus.FAILED);
    }

    @Override
    public void addStatusEntry(int ruleId, RuleStatus status)
    {
        ruleStatusService.setRuleStatus(ruleId, status);
    }

    @Override
    public void truncateLog()
    {
        // Get date one week ago
        Date oneWeekAgo = DateUtils.addWeeks(new Date(), -1);
        int deletedEntries = 0;
        log.debug("Deleting audit log entries older than: " + oneWeekAgo);

        AuditMessageEntity[] auditMessageEntities;
        do
        {
            // deleting log entries in chunks prevents possible OOM exception in case the log entries contains a huge amount of data
            // like in https://bitbucket.org/atlassianlabs/automation/issue/23/oom-due-to-verbose-error-logging-to-the
            auditMessageEntities = ao.find(AuditMessageEntity.class, Query.select().where("DATE < ?", oneWeekAgo).limit(MAX_RESULTS));
            for (AuditMessageEntity oldEntity : auditMessageEntities)
            {
                ao.delete(oldEntity.getActionMessages());
                ao.delete(oldEntity);
                deletedEntries++;
            }
            log.debug(String.format("Deleted %d audit log entries", auditMessageEntities.length));
        } while (auditMessageEntities.length == MAX_RESULTS);

        log.debug("Deleted audit log entries: " + deletedEntries);
    }
}
