package com.atlassian.plugin.automation.scheduler;

import com.atlassian.event.api.EventListener;
import com.atlassian.plugin.automation.config.Settings;
import com.atlassian.plugin.automation.core.Rule;
import com.atlassian.plugin.automation.core.TriggerContext;
import com.atlassian.plugin.automation.util.ParameterUtil;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.transaction.TransactionCallback;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.util.concurrent.ThreadPoolExecutor;

public class DefaultAutomationEventHandler implements AutomationEventHandler
{
    private static final Logger log = Logger.getLogger(DefaultAutomationEventHandler.class);

    private final ThreadPoolExecutor asyncExecutor;
    private final String eventClassName;
    private final RuleCallableContext ruleContext;
    private final PluginSettings pluginSettings;

    public DefaultAutomationEventHandler(final RuleCallableContext ruleContext,
                                         final ThreadPoolExecutor asyncExecutor,
                                         final String eventClassName,
                                         final PluginSettingsFactory pluginSettingsFactory)
    {
        this.ruleContext = ruleContext;
        this.asyncExecutor = asyncExecutor;
        this.eventClassName = eventClassName;
        this.pluginSettings = pluginSettingsFactory.createGlobalSettings();
    }

    @EventListener
    @Override
    public void handleEvent(final Object event)
    {
        if (event.getClass().getCanonicalName().equals(eventClassName))
        {
            // Event handling can be configured to be synchronous or asynchronous. Asynchronous offers better
            // overall performance but can also result in unintended consequences if the queue used by the executor
            // gets backed up under heavy system load. If the queue gets backed up, it is possible for the system to
            // experience (potentially long) delays between when events occur and when automation performs their
            // triggered actions.

            final Rule rule = ruleContext.getRule();

            if (isSyncEventHandlingEnabled(rule))
            {
                executeSync(rule, event);
            }
            else
            {
                executeAsync(rule, event);
            }
        }
    }

    private boolean isSyncEventHandlingEnabled(final Rule rule)
    {
        final String ruleEventSyncEnabled = ParameterUtil.singleValue(rule.getTriggerConfiguration().getParameters(), ParameterUtil.RULE_EVENT_SYNC_ENABLED);
        return Boolean.valueOf(StringUtils.isBlank(ruleEventSyncEnabled) ? (String) pluginSettings.get(Settings.SYNC_EVENT_HANDLING_ENABLED) : ruleEventSyncEnabled);
    }

    private void executeSync(final Rule rule, final Object event)
    {
        // Execute synchronously on the current thread
        execute(rule, event, false);
    }

    private void executeAsync(final Rule rule, final Object event)
    {
        // Submit for asynchronous execution
        asyncExecutor.submit(new Runnable()
        {
            @Override
            public void run()
            {
                execute(rule, event, true);
                log.debug("Event queue size: " + asyncExecutor.getQueue().size());
            }
        });
    }

    private void execute(final Rule rule, final Object event, final boolean prepareThread)
    {
        ruleContext.getTransactionTemplate().execute(new TransactionCallback<Object>()
        {
            @Override
            public Object doInTransaction()
            {
                try
                {
                    ruleContext.getThreadLocalExecutor().executeAs(
                            rule.getActor(),
                            new RuleCallable(ruleContext, new TriggerContext(rule.getActor(), event)),
                            prepareThread);
                }
                catch (RuntimeException e)
                {
                    log.error("Unexpected error executing automation rule '" + rule.getName() + "'", e);
                }
                return null;
            }
        });
    }
}
