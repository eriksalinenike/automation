package com.atlassian.plugin.automation.admin;

import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.websudo.WebSudoManager;
import com.atlassian.sal.api.websudo.WebSudoSessionException;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;

@Scanned
public abstract class AbstractAdminServlet extends HttpServlet
{
    protected static final String COMPLETE_PLUGIN_KEY = "com.atlassian.plugin.automation.automation-module";
    protected static final String CONFIG_RESOURCE_KEY = COMPLETE_PLUGIN_KEY + ":automation-module-config-resource";

    private static final Logger log = Logger.getLogger(AbstractAdminServlet.class);

    private final WebSudoManager webSudoManager;
    private final SoyTemplateRenderer renderer;
    private final LoginUriProvider loginUriProvider;
    private final WebResourceManager webResourceManager;

    protected final UserManager userManager;

    @Inject
    public AbstractAdminServlet(
            @ComponentImport final WebSudoManager webSudoManager,
            @ComponentImport final SoyTemplateRenderer renderer,
            @ComponentImport final UserManager userManager,
            @ComponentImport final LoginUriProvider loginUriProvider,
            @ComponentImport final WebResourceManager webResourceManager)
    {
        this.webSudoManager = webSudoManager;
        this.renderer = renderer;
        this.userManager = userManager;
        this.loginUriProvider = loginUriProvider;
        this.webResourceManager = webResourceManager;
    }

    @Override
    protected void service(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException
    {
        try
        {
            response.setContentType("text/html");
            webSudoManager.willExecuteWebSudoRequest(request);

            final String remoteUsername = userManager.getRemoteUsername(request);
            if (!userManager.isSystemAdmin(remoteUsername))
            {
                response.sendRedirect(loginUriProvider.getLoginUri(getUri(request)).toASCIIString());
                return;
            }

            requireResource(webResourceManager);
            renderResponse(renderer, request, response);
        }
        catch (WebSudoSessionException wes)
        {
            webSudoManager.enforceWebSudoProtection(request, response);
        }
        catch (SoyException e)
        {
            log.error("Exception while rendering template", e);
            response.sendError(500);
        }
    }

    protected abstract void requireResource(final WebResourceManager webResourceManager);

    protected abstract void renderResponse(final SoyTemplateRenderer renderer, final HttpServletRequest request, final HttpServletResponse response) throws IOException, SoyException;


    private URI getUri(final HttpServletRequest request)
    {
        StringBuffer builder = request.getRequestURL();
        if (request.getQueryString() != null)
        {
            builder.append("?");
            builder.append(request.getQueryString());
        }
        return URI.create(builder.toString());
    }
}
