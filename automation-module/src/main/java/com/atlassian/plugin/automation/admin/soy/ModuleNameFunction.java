package com.atlassian.plugin.automation.admin.soy;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.automation.module.AutomationActionModuleDescriptor;
import com.atlassian.plugin.automation.module.AutomationModuleManager;
import com.atlassian.plugin.automation.module.AutomationTriggerModuleDescriptor;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.soy.renderer.JsExpression;
import com.atlassian.soy.renderer.SoyClientFunction;
import com.atlassian.soy.renderer.SoyServerFunction;
import com.google.common.collect.ImmutableSet;
import org.apache.commons.lang.StringUtils;

import java.util.Set;
import javax.inject.Inject;

/**
 * Renders a plugin module's i18nized name.
 */
public class ModuleNameFunction implements SoyServerFunction<String>, SoyClientFunction
{

    private final PluginAccessor pluginAccessor;
    private final I18nResolver i18n;
    private final AutomationModuleManager automationModuleManager;

    @Inject
    public ModuleNameFunction(
            @ComponentImport final PluginAccessor pluginAccessor,
            @ComponentImport final I18nResolver i18n,
            final AutomationModuleManager automationModuleManager)
    {
        this.pluginAccessor = pluginAccessor;
        this.i18n = i18n;
        this.automationModuleManager = automationModuleManager;
    }

    @Override
    public String getName()
    {
        return "moduleName";
    }

    @Override
    public JsExpression generate(final JsExpression... args)
    {
        final StringBuilder js = new StringBuilder();
        final String moduleKeyVar = args[0].getText();
        js.append("(function() {").
                append("var ret = ").append(moduleKeyVar).append(";").
                append("var moduleNames = {};");

        for (AutomationTriggerModuleDescriptor trigger : automationModuleManager.getTriggerDescriptors())
        {
            js.append("moduleNames[\"").append(trigger.getCompleteKey()).append("\"] = \"").append(getName(trigger.getCompleteKey())).append("\";");
        }
        for (AutomationActionModuleDescriptor action : automationModuleManager.getActionDescriptors())
        {
            js.append("moduleNames[\"").append(action.getCompleteKey()).append("\"] = \"").append(getName(action.getCompleteKey())).append("\";");
        }

        js.append("if(moduleNames[").append(moduleKeyVar).append("] !== undefined) { return moduleNames[").append(moduleKeyVar).append("];}");
        js.append("return ret;})(").append(moduleKeyVar).append(")");
        return new JsExpression(js.toString());
    }

    @Override
    public String apply(final Object... args)
    {
        final String completeModuleKey = (String) args[0];
        return getName(completeModuleKey);
    }

    @Override
    public Set<Integer> validArgSizes()
    {
        return ImmutableSet.of(1);
    }

    private String getName(final String completeModuleKey)
    {
        final ModuleDescriptor<?> pluginModule = pluginAccessor.getPluginModule(completeModuleKey);
        if (pluginModule == null)
        {
            return completeModuleKey;
        }

        if (StringUtils.isNotBlank(pluginModule.getI18nNameKey()))
        {
            return i18n.getText(pluginModule.getI18nNameKey());
        }
        else if (StringUtils.isNotBlank(pluginModule.getName()))
        {
            return pluginModule.getName();
        }

        return completeModuleKey;
    }
}
