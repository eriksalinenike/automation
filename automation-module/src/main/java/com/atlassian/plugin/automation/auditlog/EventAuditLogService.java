package com.atlassian.plugin.automation.auditlog;

import com.atlassian.plugin.automation.core.auditlog.EventAuditMessage;
import com.atlassian.plugin.automation.status.RuleStatus;

/**
 * Provides access to audit log
 */
public interface EventAuditLogService extends AuditLogService
{
    /**
     * Adds given entry to the audit log
     *
     * @param eventAuditMessage the entry to add
     */
    void addEntry(EventAuditMessage eventAuditMessage);

    /**
     * Adds only rule-status related entry
     * @param ruleId id of the affected rule
     * @param status status of the rule
     */
    void addStatusEntry(int ruleId, RuleStatus status);
}
