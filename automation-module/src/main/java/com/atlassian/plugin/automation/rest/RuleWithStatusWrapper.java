package com.atlassian.plugin.automation.rest;

import com.atlassian.plugin.automation.config.DefaultRule;
import com.atlassian.plugin.automation.core.Rule;
import com.atlassian.plugin.automation.status.RuleStatus;
import com.google.common.collect.Lists;
import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Wrapper that also contains the status of a rule.
 */
public class RuleWithStatusWrapper extends DefaultRule
{
    @JsonProperty
    private final RuleStatus status;

    @JsonCreator
    public RuleWithStatusWrapper(@JsonProperty("rule") Rule rule, @JsonProperty("status") RuleStatus status)
    {
        super(rule.getId(), rule.getName(), rule.getActor(), rule.getTriggerConfiguration(), Lists.newArrayList(rule.getActionsConfiguration()), rule.isEnabled());
        this.status = status;
    }

    public RuleStatus getStatus()
    {
        return status;
    }
}
