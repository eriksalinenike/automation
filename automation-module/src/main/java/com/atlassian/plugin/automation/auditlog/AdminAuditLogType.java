package com.atlassian.plugin.automation.auditlog;

public enum AdminAuditLogType
{
    ADD_RULE("Added new rule"),
    UPDATE_RULE("Updated rule"),
    DELETE_RULE("Deleted rule"),
    ENABLE_RULE("Enabled rule"),
    DISABLE_RULE("Disabled rule"),
    AUTO_DISABLE_RULE("Auto-disabled rule"); // auto-disable rule (ie. due to possible loop)

    private String description;

    AdminAuditLogType(String description)
    {
        this.description = description;
    }

    public String getDescription()
    {
        return description;
    }

    @Override
    public String toString()
    {
        return "AdminAuditLogType{" +
                "description='" + description + '\'' +
                '}';
    }
}
