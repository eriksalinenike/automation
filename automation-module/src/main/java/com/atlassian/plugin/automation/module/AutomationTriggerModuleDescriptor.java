package com.atlassian.plugin.automation.module;

import com.atlassian.plugin.automation.core.Trigger;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.util.validation.ValidationPattern;

import static com.atlassian.plugin.util.validation.ValidationPattern.test;

public class AutomationTriggerModuleDescriptor extends AbstractModuleDescriptor<Trigger> implements Comparable<AutomationTriggerModuleDescriptor>
{
    public AutomationTriggerModuleDescriptor(final ModuleFactory moduleFactory)
    {
        super(moduleFactory);
    }

    @Override
    protected void provideValidationRules(ValidationPattern pattern)
    {
        super.provideValidationRules(pattern);
        pattern.rule(test("@class").withError("The automation trigger class is required"));
    }

    @Override
    public Trigger getModule()
    {
        return moduleFactory.createModule(moduleClassName, this);
    }

    @Override
    public int compareTo(AutomationTriggerModuleDescriptor otherDescriptor)
    {
        return this.getCompleteKey().compareTo(otherDescriptor.getCompleteKey());
    }
}
