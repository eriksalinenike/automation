package com.atlassian.plugin.automation.module;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import net.java.ao.Transaction;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

import javax.inject.Inject;
import javax.inject.Named;

import static com.atlassian.activeobjects.tx.TransactionalProxy.isAnnotated;
import static com.atlassian.activeobjects.tx.TransactionalProxy.transactional;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * This should really come from ActiveObjects directly but at the moment that means the plugin
 * will require a transform. ({@see com.atlassian.activeobjects.external.TransactionalAnnotationProcessor}
 */
@Named
public class AutomationTransactionalAnnotationProcessor implements BeanPostProcessor
{
    private final ActiveObjects ao;

    @Inject
    public AutomationTransactionalAnnotationProcessor(@ComponentImport ActiveObjects ao)
    {
        // AO-283, http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=6588239
        // prevent a sun (Oracle) JVM deadlock.
        Transaction.class.getAnnotations();


        this.ao = checkNotNull(ao);
    }

    public Object postProcessBeforeInitialization(Object o, String s) throws BeansException
    {
        return o;
    }

    public Object postProcessAfterInitialization(Object o, String s) throws BeansException
    {
        return isAnnotated(o.getClass()) ? transactional(ao, o) : o;
    }
}
