package com.atlassian.plugin.automation.module;

import com.atlassian.plugin.automation.core.Action;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.util.validation.ValidationPattern;

import static com.atlassian.plugin.util.validation.ValidationPattern.test;

public class AutomationActionModuleDescriptor extends AbstractModuleDescriptor<Action> implements Comparable<AutomationActionModuleDescriptor>
{
    public AutomationActionModuleDescriptor(final ModuleFactory moduleFactory)
    {
        super(moduleFactory);
    }

    @Override
    protected void provideValidationRules(ValidationPattern pattern)
    {
        super.provideValidationRules(pattern);
        pattern.rule(test("@class").withError("The automation action class is required"));
    }

    @Override
    public Action getModule()
    {
        return moduleFactory.createModule(moduleClassName, this);
    }

    @Override
    public int compareTo(AutomationActionModuleDescriptor otherDescriptor)
    {
        return this.getCompleteKey().compareTo(otherDescriptor.getCompleteKey());
    }
}
