package com.atlassian.plugin.automation.module;

import com.atlassian.plugin.automation.core.Action;
import com.atlassian.plugin.automation.core.Trigger;

/**
 * Keeps track of automation modules
 */
public interface AutomationModuleManager
{
    /**
     * Gets given trigger
     * @param key
     * @return
     */
    Trigger getTrigger(String key);

    /**
     * Gets given action
     * @param key
     * @return
     */
    Action getAction(String key);

    /**
     * @return All known trigger descriptors
     */
    Iterable<AutomationTriggerModuleDescriptor> getTriggerDescriptors();

    /**
     * @return All known action descriptors
     */
    Iterable<AutomationActionModuleDescriptor> getActionDescriptors();
}
