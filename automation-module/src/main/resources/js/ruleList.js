(function () {
    AJS.$(function () {
        AJS.$(".aui-buttons").on("change", "#import-rule", function (event) {
            var input = event.target;

            var reader = new FileReader();
            reader.onload = function () {
                var text = reader.result;
                var $notification = AJS.$(".aui-page-header-actions .notification");
                $notification.html(Atlassian.Templates.Automation.importWaiting());

                AJS.$.ajax({
                    url: AJS.contextPath()+'/rest/automation/1.0/rule/import',
                    data: text,
                    type: "POST",
                    contentType: "application/json",
                    success: function (resp) {
                        $notification.html(Atlassian.Templates.Automation.tickSuccess());
                        window.location = AJS.contextPath() + '/plugins/servlet/automationrules';
                    },
                    error: function (resp) {
                        var dialogContent;
                        $notification.html(Atlassian.Templates.Automation.importError());
                        try {
                            var errorsMap = JSON.parse(resp.responseText);
                            var errors=[];
                            errors.push(_.flatten(errorsMap.ruleErrors.errors));
                            errors.push(_.flatten(errorsMap.triggerErrors.errors));
                            errors.push(_.flatten(errorsMap.actionErrors.errors));
                            dialogContent = Atlassian.Templates.Automation.importErrorDialogContent({errors: _.flatten(errors)});
                        } catch (e) {
                            dialogContent = '<pre>'+AJS.format(AJS.I18n.getText("automation.plugin.error.importing.rule"), resp.responseText)+ '</pre>';
                        }
                        AJS.InlineDialog(AJS.$("#importErrorInlineDialog"), "importErrorDialog",
                            function (content, trigger, showPopup) {
                                content.css({"padding": "20px"}).html(dialogContent);
                                showPopup();
                                return false;
                            }
                        );
                    },
                    complete: function () {
                        // we need to remove and re-add the whole input as otherwise the change event won't get published if the same file is added again
                        AJS.$(input).remove();
                        AJS.$("#import-label").after(AJS.$(Atlassian.Templates.Automation.importInput()));
                    }
                });
            };
            reader.readAsText(input.files[0]);
        });
    });
})();
