AJS.namespace("Atlassian.Automation.RuleConfirmView");

Atlassian.Automation.RuleConfirmView = Brace.View.extend({
    template: Atlassian.Templates.Automation.confirm,

    events: {
        "click #save-rule-form": "saveRule"
    },

    initialize: function () {
        _.bindAll(this, 'render', 'saveRule');
    },

    render: function () {
        this.$el.html(this.template({
            rule:this.model.get("rule")
        }));
        var $detailEl = this.$(".rule-detail");
        var ruleDetail = new Atlassian.Automation.RuleDetailView({
            el:$detailEl,
            renderOps:false,
            model:this.model.get("rule")
        });
        ruleDetail.render();

        return this;
    },

    saveRule: function (e) {
        e.preventDefault();

        var $throbber = this.$(".buttons-container .throbber");
        $throbber.addClass("loading");

        if (this.model.get("copiedRule")) {
            this.model.get("rule").id = undefined;
        }
        //remove the status since otherwise the REST resource on the server complains
        this.model.get("rule").unset("status", {silent:true});

        this.model.get("rule").save({}, {
            success: function (model, resp) {
                $throbber.removeClass("loading");
                window.location.href = AJS.contextPath() + "/plugins/servlet/automationrules#rule/" + model.get("id");
            },
            error: function(model, resp) {
                if(resp.status === 400) {
                    $throbber.removeClass("loading");
                    //this should never happen but just in case!
                    alert("Validation Error: " + resp.responseText);
                } else {
                    alert(AJS.I18n.getText("automation.plugin.unknown.error.reload"));
                }
            }
        });
    }
});