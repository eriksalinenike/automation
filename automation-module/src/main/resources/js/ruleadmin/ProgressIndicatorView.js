AJS.namespace("Atlassian.Automation.ProgressIndicatorView");

Atlassian.Automation.ProgressIndicatorView = Brace.View.extend({
    tagName:"ol",
    id:"automation-progress-tracker",
    className:"aui-progress-tracker",

    template: Atlassian.Templates.Automation.progressIndicator,

    initialize: function () {
        _.bindAll(this, 'render');

        this.model.on("change:progress", this.render);
    },

    render: function () {
        this.$el.html(this.template({
            step: this.model.get("progress")
        }));

        return this;
    }
});