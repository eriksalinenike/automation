AJS.namespace("Atlassian.Automation.ActionModel");
AJS.namespace("Atlassian.Automation.ActionCollection");

Atlassian.Automation.ActionModel = Brace.Model.extend({
    namedAttributes: ["moduleKey", "params"],

    mixins: [Atlassian.Automation.Mixin.SetParams],

    defaults: {
        params: {}
    },

    initialize: function () {
    },

    parse: function (resp) {
        return {
            id: resp.id,
            moduleKey: resp.moduleKey,
            params: resp.params
        };
    }
});

Atlassian.Automation.ActionCollection = Brace.Collection.extend({
    model:Atlassian.Automation.ActionModel
});