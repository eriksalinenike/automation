AJS.namespace("Atlassian.Automation.TriggerFormView");

Atlassian.Automation.TriggerFormView = Brace.View.extend({
    template: Atlassian.Templates.Automation.triggerForm,

    mixins: [Atlassian.Automation.Mixin.ValidationErrors],

    events: {
        "change #rule-trigger-module-key": "triggerChanged"
    },

    initialize: function () {
        _.bindAll(this, 'render', 'updateModel', 'triggerChanged', 'displayErrors', 'destroy');

        this.model.get("rule").on("change", this.render);
        this.model.on("saveFormState", this.updateModel);
        this.model.on("change:errors", this.displayErrors);
    },

    render: function () {
        this.$el.html(this.template({
            id: this.model.get("rule").get("id"),
            selectedTrigger: this.model.get("rule").get("trigger").get("moduleKey"),
            triggerModules: this.model.get("triggerModules")
        }));

        this.$("#rule-trigger-module-key").change();

        return this;
    },

    updateModel: function () {
        this.model.get("rule").get("trigger").setFormParams(this.$("form").serializeObject());
    },

    triggerChanged: function () {
        var $throbber = this.$(".trigger.throbber");
        var $remoteParams = this.$(".remote-params");
        var triggerModuleKey = this.$("#rule-trigger-module-key").val();
        var trigger = this.model.get("rule").get("trigger");

        $throbber.addClass("loading");
        $remoteParams.addClass("loading-trigger");

        trigger.set("moduleKey", triggerModuleKey);
        var url = AJS.contextPath() + "/rest/automation/1.0/config/";
        var ruleModel = this.model.get("rule");
        if (!ruleModel.isNew()) {
            url += ruleModel.getId() + "/";
        }
        url += "trigger/" + triggerModuleKey;

        AJS.$.ajax({
            url: url,
            data: {actor: ruleModel.get("actor") },
            success: function (resp) {
                $throbber.removeClass("loading");
                $remoteParams.html(resp).removeClass("loading-trigger");

                AJS.$(AJS).trigger("AJS.automation.form.loaded", $remoteParams);
            }
        });
    },

    displayErrors: function () {
        this.addFormErrors(this.model.get("errors"), "triggerErrors");
    },

    destroy: function () {
        //clean up events
        this.model.get("rule").off("change", this.render);
        this.model.off("saveFormState", this.updateModel);
        this.model.off("change:errors", this.displayErrors);
        this.undelegateEvents();
    }

});